//
//  AppService.swift
//  Sailing Support
//
//  Created by Danijel on 03/12/2017.
//  Copyright © 2017 NELI_IT. All rights reserved.
//

import Foundation
import BrightFutures
import SwiftyJSON

class AppService {
    
    static let VERSION_URL = Constants.BASE_URL + "MobileMenuCategories/getLastNum"
    
    static let sharedService = AppService()
    
    var app: App!
    let locationService = LocationService()
    
    init() {
        self.app = self.loadApp()
    }
    
    func saveApp() {
        NSKeyedArchiver.archiveRootObject(self.app, toFile: self.getPath())
    }
    
    func loadApp() -> App {
        let appl: AnyObject? = NSKeyedUnarchiver.unarchiveObject(withFile: self.getPath()) as AnyObject?
        if appl == nil {
            return App()
        }
        return appl as! App // swiftlint:disable:this force_cast
    }
    
    func getPath() -> String {
        let documentsFolderPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask
            .userDomainMask, true)[0] as NSString
        return documentsFolderPath.appendingPathComponent("app_file")
    }
    
    func getLastVersion() -> Future<JSON, NSError> {
        let promise = Promise<JSON, NSError>()
        
        let req = APIRequest(endpoint: AppService.VERSION_URL, method: .get, parameters: nil, upload: false)
        APIService.sharedService.request(req)
            .onSuccess(callback: { result in
                promise.success(JSON(result))
            }).onFailure(callback: { error in
                promise.failure(error)
            }
        )
        
        return promise.future
    }
    
}
