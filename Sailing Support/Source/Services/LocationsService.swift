//
//  LocationsService.swift
//  Sailing Support
//
//  Created by Danijel on 17/01/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation
import BrightFutures
import SwiftyJSON
import Alamofire
import AlamofireObjectMapper

class LocationsService {
    
    static let GET_ALL_LOCATIONS_URL = Constants.BASE_URL  + "/MobileMenuCategories/getAllLocations"
    static let GET_LOCATIONS_URL = Constants.BASE_URL  + "MobileMenuCategories/getLocation"
    static let GET_HOME_PAGE_URL = Constants.BASE_URL + "MobileMenuCategories/getMainPageLocations"
    
    static let sharedService = LocationsService()
    
    var locationsItems: [LocationModel]?
    
    func downloadLocationsData() -> Future<Bool, NSError> {
        let promise = Promise<Bool, NSError>()
        Alamofire.request(LocationsService.GET_ALL_LOCATIONS_URL, method: .get, parameters: ["noTexts": "true"]).responseObject { (response: DataResponse<LocationRootModel>) in
            
            let locationRootModel = response.result.value

            if let items = locationRootModel?.root {
                self.locationsItems = items
                AppService.sharedService.app.locationModels = items
                AppService.sharedService.saveApp()
                promise.success(true)
            } else {
                promise.success(false)
            }
            
        }
        
        return promise.future
    }
    
    func getLocation(locationId: Int) -> Future<LocationModel, NSError> {
        let promise = Promise<LocationModel, NSError>()

        Alamofire.request(LocationsService.GET_LOCATIONS_URL, method: .get, parameters: ["locationId": locationId]).responseObject { (response: DataResponse<LocationRootModel>) in
            
            let locationRootModel = response.result.value

            if let item = locationRootModel?.root {
                promise.success(item[0])
            } else {
                promise.success(LocationModel())
            }
            
        }
        
        return promise.future
    }
    
    func getHomePageLocations() -> Future<[LocationModel], NSError> {
        let promise = Promise<[LocationModel], NSError>()
        
        Alamofire.request(LocationsService.GET_HOME_PAGE_URL).responseObject { (response: DataResponse<LocationRootModel>) in
            
            let locationRootModel = response.result.value
            
            if let items = locationRootModel?.root {
                promise.success(items)
            } else {
                promise.success([LocationModel]())
            }
            
        }
        
        return promise.future
    }

}
