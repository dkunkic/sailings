//
//  UserRoute.swift
//  Sailing Support
//
//  Created by Danijel Kunkic on 24/03/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation

class UserRoute {
    
    static let sharedService = UserRoute()
    
    var locationId: String?
    var adId: Int?
    var message: String?
    
    func setLocation(locationId: String, message: String) {
        self.locationId = locationId
        self.message = message
    }
    
    func setAdId(adId: Int) {
        self.adId = adId
    }
    
    func clear() {
        locationId = nil
        adId = nil
        message = nil
    }
    
}
