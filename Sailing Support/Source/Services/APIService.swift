//
//  ApiService.swift
//  Sailing Support
//
//  Created by Danijel on 03/12/2017.
//  Copyright © 2017 NELI_IT. All rights reserved.
//

import Foundation
import Alamofire
import BrightFutures
import SwiftyJSON

class APIService {
    
    static let sharedService = APIService()
    let sessionManager: SessionManager
    let loggingEnabeld: Bool = true
    
    init() {
        let configuraton = URLSessionConfiguration.default
        configuraton.timeoutIntervalForRequest = 20
        configuraton.timeoutIntervalForResource = 20
        self.sessionManager = SessionManager(configuration: configuraton)
    }
    
    static func sManager() -> SessionManager {
        return APIService.sharedService.sessionManager
    }
    
    func request(_ apiRequest: APIRequest) -> Future<AnyObject, NSError> {
        let promise = Promise<AnyObject, NSError>()
        
        if apiRequest.upload {
            //TODO if upload needed
        } else {
            self.requestRepeat(apiRequest: apiRequest, promise: promise, nTimes: 3)
        }
        
        return promise.future
    }
    
    func requestRepeat(apiRequest: APIRequest, promise: Promise<AnyObject, NSError>, nTimes: Int) {
        if nTimes <= 0 {
            promise.failure(NSError(domain: "Request failed", code: 0, userInfo: nil))
        } else {
            sessionManager.request(apiRequest.endpoint, method: apiRequest.method, parameters: apiRequest.parameters, encoding: JSONEncoding.default)
                .responseJSON(completionHandler: { response in
                    if let validResponse = response.response {
                        // if result is not json, but response stil good
                        if response.result.isFailure && validResponse.statusCode >= 200 && validResponse.statusCode < 300 {
                            promise.success("success" as AnyObject)
                            return
                        }
                    }
                    switch response.result {
                    case .success(let value):
                        promise.success(value as AnyObject)
                    case .failure(let error):
                        promise.failure(NSError(domain: error.localizedDescription, code: 0, userInfo: nil))
                    }
                }
            )
        }
    }
    
}
