//
//  LocationViewController.swift
//  Sailing Support
//
//  Created by Danijel on 10/02/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import youtube_ios_player_helper
import GoogleMaps
import SDWebImage
import WebKit
import MapKit

class LocationViewController: BaseViewController, WKNavigationDelegate {
    
    @IBOutlet weak var locationImageView: UIImageView!
    @IBOutlet weak var locationTitle: UILabel!
    @IBOutlet weak var introTextView: UITextView!
    @IBOutlet weak var videoContainerView: UIView!
    @IBOutlet weak var videoContainerConstraint: NSLayoutConstraint!
    @IBOutlet weak var playerView: YTPlayerView!
    @IBOutlet weak var googleMapView: GMSMapView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var phoneImageView: UIImageView!
    @IBOutlet weak var mapMarkerImageView: UIImageView!
    
    @IBOutlet weak var introTextViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var webViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var bottomContainerHeight: NSLayoutConstraint!
    
    let IMAGE_URL: String = "http://sailing.coreaplikacije.hr/"
    
    var presenter: LocationPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter?.getLocationModel()
    }
    
    func setupYoutubePlayer(youtubeLink: String) {
        if let videoID = youtubeLink.components(separatedBy: "=").last {
            playerView.delegate = self
            playerView.load(withVideoId: videoID)
        }
    }
    
    func setupVideoPlayer(videoUrl: String) {
        playerView.isHidden = true
        guard let videoPath = Bundle.main.path(forResource: "video", ofType: "avi") else { return }
        let player = AVPlayer(url: URL(fileURLWithPath: videoPath))
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = CGRect(x: 10, y: 10, width: videoContainerView.frame.width - 20, height: videoContainerView.frame.height - 20)
        videoContainerView.layer.addSublayer(playerLayer)
        player.play()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        var scriptContent = "var meta = document.createElement('meta');"
        scriptContent += "meta.name='viewport';"
        scriptContent += "meta.content='width=device-width';"
        scriptContent += "document.getElementsByTagName('head')[0].appendChild(meta);"
        
        webView.evaluateJavaScript(scriptContent, completionHandler: nil)
        webView.scrollView.isScrollEnabled = false
        
        self.webView.evaluateJavaScript("document.readyState", completionHandler: { (complete, error) in
            if complete != nil {
                self.webView.evaluateJavaScript("document.body.offsetHeight", completionHandler: { (height, error) in
                    if let cgHeight = height as? CGFloat {
                        self.webViewHeightConstraint.constant = cgHeight
                    }
                })
            }
        })
        
    }
    
    @IBAction func didTapOnmMyLocationButton(_ sender: Any) {
        
        if let location = AppService.sharedService.locationService.location {
            let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 14.0)
            
            self.googleMapView?.animate(to: camera)
            
            //Finally stop updating location otherwise it will come again and again in this delegate
            AppService.sharedService.locationService.stopUpdatingLocation()
        }
    }
    
}

extension LocationViewController: YTPlayerViewDelegate {
    
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        //playerView.playVideo()
    }
    
}

extension LocationViewController: LocationProtocol {
    
    func setupArticle(locationModel: LocationModel) {
        if let imgSrc = locationModel.imgSrc {
            locationImageView.sd_setImage(with: URL(string: String(format: "%@%@", IMAGE_URL, imgSrc.replacingOccurrences(of: "_90.", with: "_1680."))), completed: nil)
        }
        if let title = locationModel.title {
            locationTitle.text = title
        }
        if let intro = locationModel.intro {
            let style = NSMutableParagraphStyle()
            style.lineSpacing = 10
            let font = UIFont.systemFont(ofSize: 19)
            let attributes = [NSAttributedStringKey.paragraphStyle: style, NSAttributedStringKey.font: font]
            introTextView.attributedText = NSAttributedString(string: intro, attributes: attributes)
            introTextView.textColor = UIColor.darkGray
            let sizeToFitIn = CGSize(width: self.view.frame.width, height: CGFloat(MAXFLOAT))
            let newSize = introTextView.sizeThatFits(sizeToFitIn)
            introTextViewHeightConstraint.constant = newSize.height
        }
        
        if let youtubeLink = locationModel.youtube, youtubeLink != "" {
            setupYoutubePlayer(youtubeLink: youtubeLink)
        } else if let videoUrl = locationModel.video, videoUrl != "" {
            setupVideoPlayer(videoUrl: videoUrl)
        } else {
            videoContainerConstraint.constant = 0.0
        }
        
        if let galleryId = locationModel.galleryId {
            if galleryId == "0" {
                collectionViewHeight.constant = 0.0
            } else {
                presenter?.loadImages(galleryId: galleryId)
            }
        }
        
        if let text = locationModel.text {
            webView.navigationDelegate = self
            let html = "<style type=\"text/css\"> * { line-height: 30px; font-size: 20px; color: #6d727a; } </style>" + text
            webView.loadHTMLString(html, baseURL: nil)
        } else {
            webViewHeightConstraint.constant = 0.0
        }
        
        if let telephone = locationModel.phone, telephone != "" {
            phoneLabel.text = telephone
            phoneLabel.isUserInteractionEnabled = true
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapOnPhone(_:)))
            phoneLabel.addGestureRecognizer(tapGesture)
        } else {
            bottomContainerHeight.constant = 0.0
            phoneImageView.isHidden = true
            phoneLabel.text = ""
        }
        
        if let locationAddress = locationModel.address, locationAddress != "" {
            bottomContainerHeight.constant = 120
            locationLabel.text = locationAddress
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapOnLocation(_:)))
            locationLabel.isUserInteractionEnabled = true
            locationLabel.addGestureRecognizer(tapGesture)
        } else {
            mapMarkerImageView.isHidden = true
            locationLabel.text = ""
        }
    }
    
    @objc func didTapOnPhone(_ sender: UIView) {
        guard let phoneText = phoneLabel.text else { return }
        guard let number = URL(string: String(format: "tel://%@", phoneText.replacingOccurrences(of: " ", with: ""))) else { return }
        UIApplication.shared.open(number, options: [:], completionHandler: nil)
    }
    
    @objc func didTapOnLocation(_ sender: UIView) {
        guard let locationModel = presenter?.locationModel else { return }
        let coordinate = CLLocationCoordinate2DMake(locationModel.lat ?? 0.0, locationModel.lon ?? 0.0)
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary: nil))
        mapItem.name = locationModel.address ?? ""
        mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
    }
    
    func setupImages() {
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    func setupGoogleMap(locationModel: LocationModel) {
        
        if let lat = locationModel.lat, let lon = locationModel.lon {
            let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(lon)))
            let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(lon), zoom: 14.0)
            if let icon = locationModel.icon {
                let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                imageView.contentMode = .scaleAspectFit
                imageView.image = UIImage(named: String(format: "ic_%@_white", icon))
                marker.iconView = imageView
            } else {
                
            }
            marker.infoWindowAnchor = CGPoint(x: 0.5, y: 0.2)
            marker.accessibilityLabel = locationModel.locationId
            marker.map = self.googleMapView
            self.googleMapView?.animate(to: camera)
        }
        
    }
    
}

extension LocationViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter!.images.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LocationItemCell", for: indexPath) as! LocationItemCell // swiftlint:disable:this force_cast
        
        if let imageUrl = presenter!.images[indexPath.row].imageUrl {
            cell.imageView.sd_setImage(with: URL(string: String(format: "%@%@", IMAGE_URL, imageUrl).replacingOccurrences(of: "_90", with: "_160")), completed: nil)
        }
        
        return cell
    }
}

extension LocationViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let story = UIStoryboard(name: "App", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "LocationModalController") as! LocationModalController //swiftlint:disable:this force_cast
        vc.images = presenter?.images!
        vc.selectedIndex = indexPath.row
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: true, completion: nil)
    }
}
