//
//  ShareController.swift
//  Sailing Support
//
//  Created by Danijel Kunkic on 19/03/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ShareController: UIViewController {
    
    @IBOutlet weak var emailWarningLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField! {
        didSet {
            emailTextField.delegate = self
        }
    }
    @IBOutlet weak var nameWarningLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField! {
        didSet {
            nameTextField.delegate = self
        }
    }
    @IBOutlet weak var messageWarningLabel: UILabel!
    @IBOutlet weak var messageTextField: UITextField! {
        didSet {
            messageTextField.delegate = self
        }
    }
    
    var image: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
    }

    @IBAction func didTapOnSendButton(_ sender: Any) {
        
        let params: Parameters = ["email": emailTextField.text ?? "", "imgSrc": image!, "yourName": nameTextField.text ?? "", "message": messageTextField.text ?? ""]
        
        Alamofire.request("http://sailing.coreaplikacije.hr/rest/Share/insertRow", method: .post, parameters: params).responseJSON(completionHandler: { response in
            _ = response.map { json in
                let newJson = JSON(json)
                if newJson["success"].bool ?? false {
                    self.dismiss(animated: true, completion: nil)
                } else {
                    if let email = newJson["notice"]["email"].string {
                        self.emailWarningLabel.isHidden = false
                        self.emailWarningLabel.text = email
                    }
                    if let name = newJson["notice"]["yourName"].string {
                        self.nameWarningLabel.isHidden = false
                        self.nameWarningLabel.text = name
                    }
                    if let message = newJson["notice"]["message"].string {
                        self.messageWarningLabel.isHidden = false
                        self.messageWarningLabel.text = message
                    }
                    
                }
            }
        })
        
    }
    
    @IBAction func didTapCloseButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func isValidEmail(testStr: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}

extension ShareController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == emailTextField {
            emailWarningLabel.isHidden = true
        } else if textField == nameTextField {
            nameWarningLabel.isHidden = true
        } else if textField == messageTextField {
            messageWarningLabel.isHidden = true
        }
    }
}

