//
//  LocationModalController.swift
//  Sailing Support
//
//  Created by Danijel Kunkic on 19/03/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import UIKit
import FSPagerView

class LocationModalController: UIViewController {
    
    @IBOutlet weak var leftButton: UIButton! {
        didSet {
            leftButton.addTarget(self, action: #selector(didTapOnLeftButton(_:)), for: .touchUpInside)
            leftButton.imageView?.contentMode = .center
            leftButton.setImage(UIImage(named: "left")?.withRenderingMode(.alwaysTemplate), for: .normal)
            leftButton.tintColor = .white
        }
    }
    
    @IBOutlet weak var rightButton: UIButton! {
        didSet {
            rightButton.addTarget(self, action: #selector(didTapOnRightButton(_:)), for: .touchUpInside)
            rightButton.imageView?.contentMode = .center
            rightButton.setImage(UIImage(named: "right")?.withRenderingMode(.alwaysTemplate), for: .normal)
            rightButton.tintColor = .white
        }
    }
    
    @IBOutlet weak var shareButton: UIButton! {
        didSet {
            shareButton.addTarget(self, action: #selector(didTapOnShareButton(_:)), for: .touchUpInside)
            shareButton.imageView?.contentMode = .scaleAspectFit
            shareButton.setImage(UIImage(named: "share")?.withRenderingMode(.alwaysTemplate), for: .normal)
            shareButton.tintColor = .white
        }
    }

    let IMAGE_URL: String = "http://sailing.coreaplikacije.hr/"
    var images: [ImageModel]!
    var selectedIndex: Int!
    var pagerView: FSPagerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pagerView = FSPagerView(frame: self.view.frame)
        pagerView.delegate = self
        pagerView.dataSource = self
        pagerView.backgroundColor = .clear
        pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "FSPagerViewCell")
        view.addSubview(pagerView)
        view.sendSubview(toBack: pagerView)
        
        rightButton.isHidden = selectedIndex == images.count
        leftButton.isHidden = selectedIndex == 0
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        pagerView.scrollToItem(at: selectedIndex, animated: true)
    }
    
    @objc func didTapOnLeftButton(_ sender: UIButton) {
        if pagerView.currentIndex == 0 { return }
        pagerView.scrollToItem(at: pagerView.currentIndex > 0 ? pagerView.currentIndex - 1 : 0, animated: true)
    }
    
    @objc func didTapOnRightButton(_ sender: UIButton) {
        if pagerView.currentIndex == images.count - 1 { return }
        pagerView.scrollToItem(at: pagerView.currentIndex < images.count ? pagerView.currentIndex + 1 : 0, animated: true)
    }
    
    @objc func didTapOnShareButton(_ sender: UIButton) {
        let story = UIStoryboard(name: "App", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "ShareController") as! ShareController //swiftlint:disable:this force_cast
        vc.image = images[pagerView.currentIndex].imageUrl
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: true, completion: nil)
    }
}

extension LocationModalController: FSPagerViewDelegate {
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        dismiss(animated: true, completion: nil)
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        rightButton.isHidden = pagerView.currentIndex == images.count - 1
        leftButton.isHidden = pagerView.currentIndex == 0
    }
}

extension LocationModalController: FSPagerViewDataSource {
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return images.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "FSPagerViewCell", at: index)
        
        if let imageUrl = images[index].imageUrl {
            let url = String(format: "%@%@", IMAGE_URL, imageUrl).replacingOccurrences(of: "_90", with: "_600")
            cell.imageView?.contentMode = .scaleAspectFit
            cell.imageView?.sd_setImage(with: URL(string: url), completed: nil)
        }
        
        return cell
    }
    
}
