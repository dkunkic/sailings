//
//  LocationPresenter.swift
//  Sailing Support
//
//  Created by Danijel on 10/02/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation
import Alamofire

protocol LocationProtocol: class {
    func setupArticle(locationModel: LocationModel)
    func setupGoogleMap(locationModel: LocationModel)
    func setupImages()
}

class LocationPresenter {
    
    weak var view: LocationProtocol?
    var menuItem: MenuCategory!
    var images: [ImageModel]!
    var locationModel: LocationModel?
    
    init(menuItem: MenuCategory) {
        self.menuItem = menuItem
    }
    
    func getLocationModel() {
        
        LocationsService.sharedService.getLocation(locationId: self.menuItem.locationId!).onSuccess { (locationModel) in
            self.locationModel = locationModel
            self.view?.setupArticle(locationModel: locationModel)
            self.view?.setupGoogleMap(locationModel: locationModel)
        }
        return
        if let locationModels = AppService.sharedService.app.locationModels?.filter({ (locationModel) -> Bool in
            if let locationIdFirst = locationModel.locationId, let locationIdSecond = self.menuItem.locationId {
                return locationIdFirst == String(locationIdSecond)
            }
            return false
        }) {
            view?.setupArticle(locationModel: locationModels[0])
            view?.setupGoogleMap(locationModel: locationModels[0])
        }
        
    }
    
    func loadImages(galleryId: String) {
        let url = Constants.BASE_URL + "MobileMenuCategories/getGalleryImages"
        let imageParams = ["categoryId": galleryId]
        
        Alamofire.request(url, method: .get, parameters: imageParams).responseObject { (response: DataResponse<ImageModelRoot>) in
            let imageRootModel = response.result.value
            if let items = imageRootModel?.root {
                self.images = items
                self.view?.setupImages()
            }
        }
        
    }
    
}
