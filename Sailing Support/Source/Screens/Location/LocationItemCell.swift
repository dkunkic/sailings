//
//  LocationItemCell.swift
//  Sailing Support
//
//  Created by Danijel Kunkic on 25/02/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import UIKit

class LocationItemCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
}
