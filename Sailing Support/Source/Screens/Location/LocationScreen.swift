//
//  LocationScreen.swift
//  Sailing Support
//
//  Created by Danijel on 10/02/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation
import UIKit

class LocationScreen: BaseScreen {
    
    weak var mainDelegate: MainProtocol?
    var menuItem: MenuCategory!
    
    init(mainDelegate: MainProtocol, menuItem: MenuCategory) {
        self.mainDelegate = mainDelegate
        self.menuItem = menuItem
    }
    
    required init() {
        fatalError("error init")
    }
    
    override func configureViewController(_ viewController: UIViewController?) {
        if let vc = viewController as? LocationViewController {
            let presenter = LocationPresenter(menuItem: self.menuItem)
            vc.presenter = presenter
            vc.mainVCProtocol = self.mainDelegate
            presenter.view = vc
        }
    }
    
    override var storyboardName: String {
        return "App"
    }
    
    override var viewControllerStoryboardId: String {
        return "LocationViewController"
    }
    
}

