//
//  RoutesScreen.swift
//  Sailing Support
//
//  Created by Danijel on 18/02/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation
import UIKit

class RoutesScreen: BaseScreen {
    
    var mainProtocol: MainProtocol?
    
    init(mainProtocol: MainProtocol) {
        self.mainProtocol = mainProtocol
    }
    
    required init() {
        fatalError("init() has not been implemented")
    }
    
    override func configureViewController(_ viewController: UIViewController?) {
        if let vc = viewController as? RoutesViewController {
            vc.mainProtocol = self.mainProtocol
            let presenter = RoutesPresenter()
            vc.presenter = presenter
            presenter.view = vc
        }
    }
    
    override var storyboardName: String {
        return "App"
    }
    
    override var viewControllerStoryboardId: String {
        return "RoutesViewController"
    }
    
}
