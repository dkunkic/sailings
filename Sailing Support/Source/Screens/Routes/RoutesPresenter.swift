//
//  RoutesPresenter.swift
//  Sailing Support
//
//  Created by Danijel on 18/02/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation
import Alamofire

protocol RoutesProtocol: class {
    func setupRoutes(routes: [RouteModel])
}

class RoutesPresenter {
    
    weak var view: RoutesProtocol?
    var routes: [RouteModel]?
    
    func setup() {
        
        loadRoutes()
        
    }
    
    func loadRoutes() {
        Alamofire.request("http://sailing.coreaplikacije.hr/rest/MobileMenuCategories/getAllRoutes").responseObject { (response: DataResponse<RoutesRootModel>) in
            let rootModel = response.result.value
            if let items = rootModel?.root {
                self.routes = items
                self.view?.setupRoutes(routes: items)
            }
        }
    }
}
