//
//  RoutesViewController.swift
//  Sailing Support
//
//  Created by Danijel on 18/02/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps

class RoutesViewController: BaseViewController {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var presenter: RoutesPresenter!
    
    var mainProtocol: MainProtocol?
    
    var scrollViewHeight: CGFloat = 0
    var polyline: GMSPolyline?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.setup()
        setupGoogleMap()
    }
    
    func getRouteView(routeModel: RouteModel) -> UIView {
        guard let desc = routeModel.desc else { return UIView(frame: CGRect(x: 0, y: 0, width: stackView.frame.width, height: 1)) }
        let labelHeight = heightForLabel(text: desc, font: UIFont.systemFont(ofSize: 16), width: stackView.frame.width - 30)
        let routeView = UIView(frame: CGRect(x: 0, y: 10, width: stackView.frame.width, height: 35 + labelHeight + 35))
        
        //routeView.widthAnchor.constraint(equalToConstant: stackView.frame.width).isActive = true
        routeView.heightAnchor.constraint(equalToConstant: 35 + labelHeight + 35).isActive = true
        routeView.backgroundColor = UIColor.black
        
        routeView.addSubview(getRouteTitle(routeName: routeModel.name, width: routeView.frame.width))
        routeView.addSubview(getRouteDesc(routeDesc: desc, height: labelHeight, width: routeView.frame.width))
        
        let view = UIView(frame: CGRect(x: 15, y: 35 + labelHeight + 10, width: routeView.frame.width - 30, height: 1))
        view.backgroundColor = UIColor.white
        routeView.addSubview(view)
        
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: routeView.frame.width, height: routeView.frame.height))
        button.setTitle("", for: .normal)
        if let id = routeModel.routeId {
            button.tag = Int(id)!
        }
        button.addTarget(self, action: #selector(tapOnRow(sender:)), for: .touchUpInside)
        routeView.addSubview(button)
        
        scrollViewHeight += routeView.frame.height
        
        return routeView
    }
    
    @objc func tapOnRow(sender: UIButton) {
        mapView.clear()
        guard let routes = presenter.routes else { return }
        guard let route = routes.first(where: {
            if let routeId = $0.routeId {
                return routeId == String(format: "%d", sender.tag)
            }
            return false
        }) else { return }
        guard let points = route.points else { return }

        let path = GMSMutablePath()
        var bounds = GMSCoordinateBounds()
        for point in points {
            guard let latitude = point.lat, let longitude = point.lon else { continue }
            let coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude))
            path.add(coordinate)
            bounds = bounds.includingCoordinate(coordinate)
            
            if let i =  AppService.sharedService.app.locationModels?.index(where: { locationModel in
                guard let id = locationModel.locationId else { return false }
                guard let secId = point.locationId else { return false }
                return id == secId
            }) {
                guard let locationModel = AppService.sharedService.app.locationModels?[i] else { return }
                let position = CLLocationCoordinate2D(latitude: CLLocationDegrees(locationModel.lat!), longitude: CLLocationDegrees(locationModel.lon!))
                let marker = GMSMarker(position: position)
                if let icon = locationModel.icon {
                    let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                    imageView.contentMode = .scaleAspectFit
                    imageView.image = UIImage(named: String(format: "ic_%@_white", icon))
                    marker.iconView = imageView
                    
                }
                marker.infoWindowAnchor = CGPoint(x: 0.5, y: 0.2)
                marker.accessibilityLabel = locationModel.locationId
                marker.map = mapView
            }
        }
        if polyline == nil {
            polyline = GMSPolyline(path: path)
        } else {
            polyline?.map = nil
            polyline?.path = path
        }
        
        polyline?.strokeWidth = 2.0
        polyline?.strokeColor = .red
        polyline?.map = mapView
        mapView.animate(with: GMSCameraUpdate.fit(bounds))

    }
    
    @IBAction func didTapOnUserLocation(_ sender: Any) {
        if let location = AppService.sharedService.locationService.location {
            let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 14.0)
            
            self.mapView?.animate(to: camera)
            
            //Finally stop updating location otherwise it will come again and again in this delegate
            AppService.sharedService.locationService.stopUpdatingLocation()
        }
    }
    
    func getRouteTitle(routeName: String?, width: CGFloat) -> UILabel {
        let nameLabel = UILabel(frame: CGRect(x: 15, y: 0, width: width - 30, height: 45))
        nameLabel.textColor = UIColor.white
        nameLabel.numberOfLines = 0
        nameLabel.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        if let name = routeName {
            nameLabel.text = name
        }
        return nameLabel
    }
    
    func getRouteDesc(routeDesc: String, height: CGFloat, width: CGFloat) -> UILabel {
        let label: UILabel = UILabel(frame: CGRect(x: 15, y: 45, width: width - 30, height: height))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 16)
        label.text = routeDesc
        return label
    }
    
    func heightForLabel(text: String, font: UIFont, width: CGFloat) -> CGFloat {
        let label: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        
        return label.frame.height
    }
    
    func setupGoogleMap() {
        if let location = AppService.sharedService.locationService.location {
            let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 14.0)
            
            self.mapView?.animate(to: camera)
            
            //Finally stop updating location otherwise it will come again and again in this delegate
            AppService.sharedService.locationService.stopUpdatingLocation()
        }
        
        mapView.mapType = .satellite
        mapView.isMyLocationEnabled = true
        mapView.delegate = self
    }
    
    
    
}

extension RoutesViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        let id = marker.accessibilityLabel
        if let markerInfoView = MapMarkerInfoView.instanceFromNib() as? MapMarkerInfoView {
            if let i =  AppService.sharedService.app.locationModels?.index(where: { locationModel in
                guard let idSec = locationModel.locationId else { return false }
                return id == idSec
            }) {
                if let locationModel = AppService.sharedService.app.locationModels?[i] {
                    markerInfoView.label.text = locationModel.title
                }
            }
            return markerInfoView
        }
        
        return UIView()
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        let id = marker.accessibilityLabel
        if let i =  AppService.sharedService.app.locationModels?.index(where: { locationModel in
            guard let idSec = locationModel.locationId else { return false }
            return id == idSec
        }) {
            guard let locationModel = AppService.sharedService.app.locationModels?[i] else { return }
            if let categoryId = locationModel.categoryId {
                let menuItem = MenuCategory(categoryName: "", categoryId: Int(categoryId)!, icon: "")
                menuItem.locationId = Int(locationModel.locationId ?? "1")
                guard let vc = LocationScreen(mainDelegate: self.mainProtocol!, menuItem: menuItem).instantiateViewController() else { return }
                self.mainProtocol?.openScreen(viewController: vc)
            }
        }
    }
}

extension RoutesViewController: RoutesProtocol {
    
    func setupRoutes(routes: [RouteModel]) {
        for routeModel in routes {
            stackView.addArrangedSubview(getRouteView(routeModel: routeModel))
        }
        let view = UIView()
        view.heightAnchor.constraint(equalToConstant: 64).isActive = true
        stackView.addArrangedSubview(view)
        scrollView.contentSize = CGSize(width: stackView.frame.width, height: scrollView.frame.height + 64)
    }
    
}
