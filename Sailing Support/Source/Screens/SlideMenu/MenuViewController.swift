//
//  MenuViewController.swift
//  Sailing Support
//
//  Created by Danijel on 01/12/2017.
//  Copyright © 2017 NELI_IT. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    private struct AssociatedKeys {
        static var padding = UIEdgeInsets()
    }
    
    public var padding: UIEdgeInsets? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.padding) as? UIEdgeInsets
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(self, &AssociatedKeys.padding, newValue as UIEdgeInsets!, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
    
    override open func draw(_ rect: CGRect) {
        if let insets = padding {
            self.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
        } else {
            self.drawText(in: rect)
        }
    }
    
    override open var intrinsicContentSize: CGSize {
        guard let text = self.text else { return super.intrinsicContentSize }
        
        var contentSize = super.intrinsicContentSize
        var textWidth: CGFloat = frame.size.width
        var insetsHeight: CGFloat = 0.0
        
        if let insets = padding {
            textWidth -= insets.left + insets.right
            insetsHeight += insets.top + insets.bottom
        }
        
        let newSize = text.boundingRect(with: CGSize(width: textWidth, height: CGFloat.greatestFiniteMagnitude),
                                        options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                        attributes: [NSAttributedStringKey.font: self.font], context: nil)
        
        contentSize.height = ceil(newSize.size.height) + insetsHeight
        
        return contentSize
    }
}

class MenuViewController: UIViewController {
    
    weak var mainProtocol: MainProtocol?
    var presenter: MenuPresenter!
    var selectedTableViewCell: Int?
    var mainStackView: UIStackView!
    
    var stackViewHeight: Int = 0
    
    @IBOutlet weak var menuScrollView: UIScrollView!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = MenuPresenter(view: self)
        setupMenuItems()
    }
    
    func setupMenuItems() {
        self.stackViewHeight = 220 + (presenter.getFixedMenuItems().count - 1) * 50 + (presenter.getMenuItems().count * 50)
        self.mainStackView = UIStackView(frame: CGRect(x: 0, y: 0, width: Int(UIScreen.main.bounds.width - 80), height: self.stackViewHeight))
        self.mainStackView.axis = .vertical
        self.mainStackView.distribution = .equalCentering
        self.menuScrollView.addSubview(self.mainStackView)
        self.menuScrollView.contentSize = CGSize(width: UIScreen.main.bounds.width - 80, height: self.mainStackView.frame.height)
        let fixedItems = presenter.getFixedMenuItems()
        for (index, fixedMenuCategory) in fixedItems.enumerated() {
            if index == 0 {
                createWeatherRow()
            } else {
                createRowForFixedItems(menuCategory: fixedMenuCategory, index: index)
            }
        }
        
        for (index, itemChild) in presenter.getMenuItems().enumerated() {
            createRowForFixedItems(menuCategory: itemChild, index: index)
        }
    }
    
    func createWeatherRow() {
        guard let weatherView = Bundle.main.loadNibNamed("WeatherView", owner: nil, options: nil)?.first as? WeatherView else { return }
        weatherView.heightAnchor.constraint(equalToConstant: 220).isActive = true
        
        WeatherService.sharedService.getHourItems().onSuccess { items in
            if let icon = items[0].weatherIcon {
                weatherView.iconImageView.image = UIImage(named: String(format: "w_%d", icon))
            }
            if let locationName = WeatherService.sharedService.localizedName {
                weatherView.locationNameLabel.text = locationName
            }
            if let currentWeather = items[0].iconPhrase {
                weatherView.currentWeather.text = currentWeather
            }
        }
        
        self.mainStackView.addArrangedSubview(weatherView)
    }
    
    func createRowForFixedItems(menuCategory: MenuCategory, index: Int) {
        guard let resizableView = Bundle.main.loadNibNamed("ResizableView", owner: nil, options: nil)?.first as? ResizableView else { return }
        resizableView.viewHeightAnchorConstraint = resizableView.heightAnchor.constraint(equalToConstant: 50)
        resizableView.viewHeightAnchorConstraint?.isActive = true
        resizableView.stackView.isHidden = true
        resizableView.categoryTitleLabel.text = menuCategory.categoryName
        resizableView.menuItem = menuCategory
        guard let icon = menuCategory.icon else { return }
        resizableView.view.backgroundColor = UIColor.init(red: 53/255, green: 61/255, blue: 72/255, alpha: 1)
        resizableView.imageView.image = UIImage(named: icon)
        resizableView.categoryTitleLabel.textColor = UIColor.white
        resizableView.button.tag = menuCategory.categoryId!
        resizableView.button.addTarget(self, action: #selector(tapOnRow(sender:)), for: .touchUpInside)
        self.mainStackView.addArrangedSubview(resizableView)
    }
    
    func createChildItem(item: MenuCategory, iterationNum: Int) -> UIView {
        guard let resizableView = Bundle.main.loadNibNamed("ResizableView", owner: nil, options: nil)?.first as? ResizableView else { return UIView() }
        resizableView.viewHeightAnchorConstraint = resizableView.heightAnchor.constraint(equalToConstant: 50)
        resizableView.viewHeightAnchorConstraint?.isActive = true
        resizableView.backgroundColor = UIColor.gray
        
        resizableView.button.addTarget(self, action: #selector(tapOnRow(sender:)), for: .touchUpInside)
        resizableView.button.tag = item.categoryId!
        
        resizableView.categoryTitleLabel.padding = UIEdgeInsets(top: 0, left: CGFloat(20 + (20 * iterationNum)), bottom: 0, right: 0)
        resizableView.iterationNum = iterationNum + 1
        resizableView.categoryTitleLabel.text = item.categoryName
        
        resizableView.menuItem = item
        
        return resizableView
    }
    
    @objc func tapOnRow(sender: UIButton) {     // swiftlint:disable:this cyclomatic_complexity
        var child = getChildFromItems(items: presenter.getFixedMenuItems(), id: sender.tag)
        if child == nil {
            child = getChildFromItems(items: presenter.getMenuItems(), id: sender.tag)
        } else {
            if let child = child, let categoryId = child.categoryId {
                switch categoryId {
                case -1:
                    guard let vc = HomeScreen(mainDelegate: self.mainProtocol!).instantiateViewController() else { return }
                    self.mainProtocol?.openScreen(viewController: vc)
                    self.mainProtocol?.openCloseMenu()
                    return
                case -2:
                    guard let vc = WeatherForecastScreen(mainDelegate: self.mainProtocol!).instantiateViewController() else { return }
                    self.mainProtocol?.openScreen(viewController: vc)
                    self.mainProtocol?.openCloseMenu()
                    return
                case -3:
                    guard let vc = NavigationSailingScreen().instantiateViewController() else { return }
                    self.mainProtocol?.openScreen(viewController: vc)
                    self.mainProtocol?.openCloseMenu()
                    return
                case -4:
                    break
                case -5:
                    break
                case -6:
                    openSkyMap()
                    return
                case -7:
                    openISSTracker()
                    return
                case -8:
                    guard let vc = RoutesScreen(mainProtocol: mainProtocol!).instantiateViewController() else { return }
                    self.mainProtocol?.openScreen(viewController: vc)
                    self.mainProtocol?.openCloseMenu()
                    return
                    default:
                        return
                }
            }
        }
        
        guard let safeChild = child else { return }
        
        guard let resizableView = sender.superview as? ResizableView else { return }
        
        if let menuCategory = resizableView.menuItem, menuCategory.categoryId! > 0 {
            if menuCategory.locationId != nil && menuCategory.locationId != -1 {
                if let _ = menuCategory.categoryId {
                    guard let vc = LocationScreen(mainDelegate: self.mainProtocol!, menuItem: menuCategory).instantiateViewController() else { return }
                    self.mainProtocol?.openScreen(viewController: vc)
                    self.mainProtocol?.openCloseMenu()
                    return
                }
            } else {
                if let _ = menuCategory.categoryId {
                    guard let vc = LocationsMapScreen(mainDelegate: self.mainProtocol!, menuItem: menuCategory).instantiateViewController() else { return }
                    self.mainProtocol?.openScreen(viewController: vc)
                }
            }
        }
        guard let safeChildren = safeChild.children else {
            self.mainProtocol?.openCloseMenu()
            return
        }
        if safeChildren.count == 0 {
            self.mainProtocol?.openCloseMenu()
            return
        }
        didTapOnResizableViewWithChild(children: safeChildren, childrenCount: safeChildren.count, resizableView: resizableView)
    }
    
    func openSkyMap() {
        
        UIApplication.shared.open(URL(string: "skyview-lite://")!, options: [:], completionHandler: { success in
        if !success {
            UIApplication.shared.open(URL(string: "https://itunes.apple.com/us/app/skyview-lite/id413936865")!, options: [:], completionHandler: nil)
        }
        })
    }
    
    func openISSTracker() {
        UIApplication.shared.open(URL(string: "iss-spotter")!, options: [:], completionHandler: { success in
            if !success {
                UIApplication.shared.open(URL(string: "https://itunes.apple.com/us/app/iss-spotter/id523486350")!, options: [:], completionHandler: nil)
            }
        })
    }
    
    func getChildFromItems(items: [MenuCategory]?, id: Int) -> MenuCategory? {
        guard let children = items else { return nil }
        for child in children {
            if child.categoryId == id {
                return child
            }
            let child = getChildFromItems(items: child.children, id: id)
            if child != nil { return child }
        }
        return nil
    }
    
    func didTapOnResizableViewWithChild(children: [MenuCategory], childrenCount: Int, resizableView: ResizableView) {
        if resizableView.isExtended {
            for subview in resizableView.stackView.subviews {
                subview.removeFromSuperview()
            }
        } else {
            for smallChild in children {
                resizableView.stackView.addArrangedSubview(createChildItem(item: smallChild, iterationNum: resizableView.iterationNum))
            }
        }
        var newMainStackHeight: CGFloat = 0
        var newRowItemHeight: CGFloat = 0
        if resizableView.isExtended {
            newRowItemHeight = 50
            newMainStackHeight = self.mainStackView.frame.height - resizableView.frame.height + newRowItemHeight
        } else {
            newRowItemHeight = CGFloat(Int(resizableView.frame.size.height) + (childrenCount * 50))
            newMainStackHeight = self.mainStackView.frame.height + newRowItemHeight - 50
        }
        resizableView.viewHeightAnchorConstraint?.constant = newRowItemHeight
        resizableView.previousHeight = newRowItemHeight
        resizableView.isExtended = !resizableView.isExtended
        changeReizableViewParentsHeight(resizableView: resizableView, newRowItemHeight: newRowItemHeight)
        resizableView.stackView.isHidden = !resizableView.isExtended
        self.mainStackView.frame = CGRect(x: self.mainStackView.frame.origin.x,
                                          y: self.mainStackView.frame.origin.y,
                                          width: self.mainStackView.frame.width,
                                          height: newMainStackHeight)
        self.menuScrollView.contentSize = CGSize(width: self.mainStackView.frame.width, height: self.mainStackView.frame.height + 20)
    }
    
    func changeReizableViewParentsHeight(resizableView: ResizableView, newRowItemHeight: CGFloat) {
        guard let parentView = resizableView.superview?.superview as? ResizableView else {
            return
        }
        parentView.viewHeightAnchorConstraint?.constant = resizableView.isExtended ? parentView.previousHeight + (resizableView.viewHeightAnchorConstraint?.constant)! - 50 : parentView.previousHeight
        if (parentView.superview?.superview as? ResizableView) != nil {
            changeReizableViewParentsHeight(resizableView: parentView, newRowItemHeight: newRowItemHeight)
        }
        
    }
    
    // MARK: - Interaction
    var interactor: Interactor?
    
    @IBAction func handleGesture(sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: view)
        let progress = MenuHelper.calculateProgress(
            translationInView: translation,
            viewBounds: view.bounds,
            direction: .right
        )
        if progress > 0.09 {
            mainProtocol?.openCloseMenu()
        }
        
    }
    
    @IBAction func closeMenu(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

// MARK: - MenuPresenterProtocol
extension MenuViewController: MenuPresenterProtocol {
    
    func openScreen(viewController: UIViewController) {
        mainProtocol?.openScreen(viewController: viewController)
    }
    
    func itemsReady() {
        //menuTableView.reloadData()
    }
    
}
