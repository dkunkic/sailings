//
//  Interactor.swift
//  Sailing Support
//
//  Created by Danijel on 01/12/2017.
//  Copyright © 2017 NELI_IT. All rights reserved.
//

import UIKit

class Interactor: UIPercentDrivenInteractiveTransition {
    var hasStarted = false
    var shouldFinish = false
}
