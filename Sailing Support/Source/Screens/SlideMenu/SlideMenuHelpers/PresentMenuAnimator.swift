//
//  PresentMenuAnimator.swift
//  Sailing Support
//
//  Created by Danijel on 01/12/2017.
//  Copyright © 2017 NELI_IT. All rights reserved.
//

import UIKit

class PresentMenuAnimator: NSObject {
    
    var fromVC: UIViewController?
    var toVC: UIViewController?
    var containerViewSnapshot: UIView?
    
    func changeSnapshot(view: UIView) {
        guard let snapshot = view.snapshotView(afterScreenUpdates: false) else { return }
        snapshot.tag = MenuHelper.snapshotNumber
        snapshot.isUserInteractionEnabled = false
        snapshot.layer.shadowOpacity = 0.7
        
        guard let containerView = containerViewSnapshot else { return }
        containerView.insertSubview(snapshot, aboveSubview: self.toVC!.view)
        fromVC!.view.isHidden = true
    }
    
}

extension PresentMenuAnimator: UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.6
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard
            let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from),
            let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
            else { return }
        
        self.fromVC = fromVC
        self.toVC = toVC
        
        self.containerViewSnapshot = transitionContext.containerView
        
        if let containerView = self.containerViewSnapshot {
            containerView.insertSubview(toVC.view, belowSubview: fromVC.view)
            
            guard let snapshot = fromVC.view.snapshotView(afterScreenUpdates: false) else { return }
            snapshot.tag = MenuHelper.snapshotNumber
            snapshot.isUserInteractionEnabled = false
            snapshot.layer.shadowOpacity = 0.7
            containerView.insertSubview(snapshot, aboveSubview: toVC.view)
            fromVC.view.isHidden = true
            
            UIView.animate(
                withDuration: transitionDuration(using: transitionContext),
                animations: {
                    snapshot.center.x += UIScreen.main.bounds.width * MenuHelper.menuWidth
            },
                completion: { _ in
                    fromVC.view.isHidden = false
                    transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            }
            )
        }
    }
}
