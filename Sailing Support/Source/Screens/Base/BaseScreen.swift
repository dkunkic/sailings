//
//  BaseScreen.swift
//  Sailing Support
//
//  Created by Danijel on 01/12/2017.
//  Copyright © 2017 NELI_IT. All rights reserved.
//

import Foundation
import UIKit

class BaseScreen {
    
    required init() {
        
    }
    
    func push(_ navigationController: UINavigationController?) {
        if let vc = self.instantiateViewController() {
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func present(_ presentingViewController: UIViewController) {
        if let vc = self.instantiateViewController() {
            presentingViewController.present(vc, animated: false, completion: nil)
        }
    }
    
    func instantiateViewController () -> UIViewController? {
        let sb = UIStoryboard(name: self.storyboardName, bundle: Bundle.main)
        let vc = sb.instantiateViewController(withIdentifier: self.viewControllerStoryboardId)
        self.configureViewController(vc)
        return vc
    }
    
    var storyboardName: String {
        return "Main"
    }
    
    var viewControllerStoryboardId: String {
        return ""
    }
    
    func configureViewController(_ viewController: UIViewController?) {
        
    }
    
}
