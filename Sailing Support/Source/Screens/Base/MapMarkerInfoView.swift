//
//  MapMarkerInfoView.swift
//  Sailing Support
//
//  Created by Danijel Kunkic on 28/02/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import UIKit

class MapMarkerInfoView: UIView {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var button: UIButton!
    
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "MapMarkerInfoView", bundle: nil).instantiate(withOwner: self, options: nil).first as! MapMarkerInfoView // swiftlint:disable:this force_cast
    }
    
}
