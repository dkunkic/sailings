//
//  LocationsMapPresenter.swift
//  Sailing Support
//
//  Created by Danijel on 20/01/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation

protocol LocationsMapProtocol: class {
    func setupImageAndTitle(menuItem: MenuCategory)
    func setupGoogleMap(locationModels: [LocationModel])
}

class LocationsMapPresenter {
    
    weak var view: LocationsMapProtocol?
    var menuItem: MenuCategory!
    
    init(menuItem: MenuCategory) {
        self.menuItem = menuItem
    }
    
    func getLocationModel() {
        view?.setupImageAndTitle(menuItem: menuItem)
        if let locationModels = AppService.sharedService.app.locationModels?.filter({ (locationModel) -> Bool in
            if let categoryIdFirst = locationModel.categoryId, let categoryIdSecond = self.menuItem.categoryId {
                return categoryIdFirst == String(categoryIdSecond)
            }
            return false
        }) {
            view?.setupGoogleMap(locationModels: locationModels)
        }

    }
    
}
