//
//  LocationsMap.swift
//  Sailing Support
//
//  Created by Danijel on 20/01/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import SDWebImage

class LocationsMapViewController: BaseViewController {
    
    let IMAGE_URL: String = "http://sailing.coreaplikacije.hr/"
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var googleMapView: GMSMapView!
    
    var presenter: LocationsMapPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter?.getLocationModel()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func didTapOnUserLocation(_ sender: Any) {
        if let location = AppService.sharedService.locationService.location {
            let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 14.0)
            
            self.googleMapView?.animate(to: camera)
            
            //Finally stop updating location otherwise it will come again and again in this delegate
            AppService.sharedService.locationService.stopUpdatingLocation()
        }
    }
    
    
}

extension LocationsMapViewController: LocationsMapProtocol {
    
    func setupImageAndTitle(menuItem: MenuCategory) {
        if let imgSrc = menuItem.imgSrc {
            imageView.sd_setImage(with: URL(string: String(format: "%@%@", IMAGE_URL, imgSrc.replacingOccurrences(of: "_90.", with: "_1680."))), completed: nil)
        }
        if let title = menuItem.categoryName {
            titleLabel.text = title
        }
    }
    
    func setupGoogleMap(locationModels: [LocationModel]) {
//        if let location = AppService.sharedService.locationService.location {
//            let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 14.0)
//
//            self.googleMapView?.animate(to: camera)
//
//            //Finally stop updating location otherwise it will come again and again in this delegate
//            AppService.sharedService.locationService.stopUpdatingLocation()
//        }

        googleMapView.mapType = .satellite
        googleMapView.isMyLocationEnabled = true
        googleMapView.delegate = self
        
        var bounds = GMSCoordinateBounds()
        for locationModel in locationModels {
            if let lat = locationModel.lat, let lon = locationModel.lon {
                let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(lon)))
                if let icon = locationModel.icon {
                    let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                    imageView.contentMode = .scaleAspectFit
                    imageView.image = UIImage(named: String(format: "ic_%@_white", icon))
                    marker.iconView = imageView
                }
                marker.tracksViewChanges = false
                marker.infoWindowAnchor = CGPoint(x: 0.5, y: 0.2)
                marker.accessibilityLabel = locationModel.locationId
                marker.map = self.googleMapView
                
                bounds = bounds.includingCoordinate(marker.position)
            }
        }
        
        googleMapView.animate(with: GMSCameraUpdate.fit(bounds))
        
        if locationModels.count <= 1 {
            self.googleMapView?.animate(toZoom: 14)
        }
    }
    
}


extension LocationsMapViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        let id = marker.accessibilityLabel
        if let markerInfoView = MapMarkerInfoView.instanceFromNib() as? MapMarkerInfoView {
            if let i =  AppService.sharedService.app.locationModels?.index(where: { locationModel in
                guard let idSec = locationModel.locationId else { return false }
                return id == idSec
            }) {
                if let locationModel = AppService.sharedService.app.locationModels?[i] {
                    markerInfoView.label.text = locationModel.title
                }
            }
            return markerInfoView
        }
        
        return UIView()
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        let id = marker.accessibilityLabel
        if let i =  AppService.sharedService.app.locationModels?.index(where: { locationModel in
            guard let idSec = locationModel.locationId else { return false }
            return id == idSec
        }) {
            guard let locationModel = AppService.sharedService.app.locationModels?[i] else { return }
            if let categoryId = locationModel.categoryId {
                let menuItem = MenuCategory(categoryName: "", categoryId: Int(categoryId)!, icon: "")
                menuItem.locationId = Int(locationModel.locationId ?? "1")
                guard let vc = LocationScreen(mainDelegate: self.mainVCProtocol!, menuItem: menuItem).instantiateViewController() else { return }
                self.mainVCProtocol?.openScreen(viewController: vc)
            }
        }
    }
}
