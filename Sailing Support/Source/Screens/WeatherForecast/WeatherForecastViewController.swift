//
//  WeatherForecastViewController.swift
//  Sailing Support
//
//  Created by Danijel on 11/02/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import UIKit
import WebKit
import MapKit

class WeatherForecastCell: UICollectionViewCell {
    @IBOutlet weak var dayImageView: UIImageView!
    @IBOutlet weak var nightImageView: UIImageView!
    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
}

class WeatherForecastViewController: BaseViewController {
    
    @IBOutlet weak var firstWebKitView: WKWebView!
    @IBOutlet weak var secWebKitView: WKWebView!
    
    @IBOutlet weak var locationTitleLabel: UILabel!
    @IBOutlet weak var locationWeatherDescLabel: UILabel!
    @IBOutlet weak var weatherIconImage: UIImageView!
    
    //weather
    @IBOutlet weak var tempBigLabel: UILabel!
    @IBOutlet weak var smallTempLabel: UILabel!
    @IBOutlet weak var feelsLikeTempLabel: UILabel!
    @IBOutlet weak var windSpeed: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var presenter: WeatherForecastPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter?.setup()
    }
    
}

extension WeatherForecastViewController: WeatherForecastProtocol {
    
    func loadWebViews(location: CLLocation) {
        let urlFirst = String(format: "https://embed.windy.com/embed2.html?lat=%f&lon=%f&zoom=9&level=surface&overlay=wind&menu=&message=&marker=&forecast=12&calendar=now&location=coordinates&type=map&actualGrid=&metricWind=kt&metricTemp=%%C2%%B0C", location.coordinate.latitude, location.coordinate.longitude)
        let urlSec = String(format: "https://embed.windy.com/embed2.html?lat=%f&lon=%f&zoom=9&level=surface&overlay=rain&menu=&message=&marker=&forecast=12&calendar=now&location=coordinates&type=map&actualGrid=&metricWind=kt&metricTemp=%%C2%%B0C", location.coordinate.latitude, location.coordinate.longitude)
        
        if let url =  URL(string: urlFirst) {
            firstWebKitView.load(URLRequest(url: url))
        }
        if let url =  URL(string: urlSec) {
            secWebKitView.load(URLRequest(url: url))
        }
    }
    
    func setupCurrentDayItem(weatherItem: WeatherHourItem, locationName: String) {
        locationTitleLabel.text = locationName.uppercased()
        if let currentWeather = weatherItem.iconPhrase {
            locationWeatherDescLabel.text = currentWeather
        }
        if let icon = weatherItem.weatherIcon {
            weatherIconImage.image = UIImage(named: String(format: "w_%d", icon))
        }
        if let temp = weatherItem.temperature,
            let value = temp.value {
            tempBigLabel.text = String(format: "%.1f °C", value)
            smallTempLabel.text = String(format: "%.1f °C", value)
        }
        if let temp = weatherItem.realFeelTemperature,
            let value = temp.value {
            feelsLikeTempLabel.text = String(format: "Feels like %.1f °C", value)
        }
        if let windSpeedA = weatherItem.wind?.speed?.value {
            if let windDirectionAngle = weatherItem.wind?.direction?.degrees {
                if let windDirection = weatherItem.wind?.direction?.localized {
                    windSpeed.text = String(format: "%.1f km/h %.1f ° %@", windSpeedA, windDirectionAngle, windDirection)
                }
            }
        }
    }
    
    func setupDays() {
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
}

extension WeatherForecastViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let items = presenter?.dayItems else { return 0 }
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WeatherForecastCell", for: indexPath) as! WeatherForecastCell // swiftlint:disable:this force_cast
        
        guard let dayItems = presenter?.dayItems else { return cell }
        let dayItem = dayItems[indexPath.row]
        
        if let dayIcon = dayItem.weatherDay?.icon {
            cell.dayImageView.image = UIImage(named: String(format: "w_%d", dayIcon))
        }
        if let nightIcon = dayItem.weatherNight?.icon {
            cell.nightImageView.image = UIImage(named: String(format: "w_%d", nightIcon))
        }
        
        if let minTemp = dayItem.temperature?.minimum?.value {
            cell.minTempLabel.text = String(format: "Min %.1f °C", minTemp)
        }
        if let maxTemp = dayItem.temperature?.maximum?.value {
            cell.maxTempLabel.text = String(format: "Max %.1f °C", maxTemp)
        }
        
        if let windSpeedA = dayItem.weatherDay?.wind?.speed?.value {
            if let windDirectionAngle = dayItem.weatherDay?.wind?.direction?.degrees {
                if let windDirection = dayItem.weatherDay?.wind?.direction?.localized {
                    cell.windLabel.text = String(format: "%.1f km/h %.1f ° %@", windSpeedA, windDirectionAngle, windDirection)
                }
            }
        }
        
        if let epochDateTime = dayItem.epochDate {
            let date = Date(timeIntervalSince1970: TimeInterval(epochDateTime))
            let dateFormater = DateFormatter()
            dateFormater.dateFormat = "EEE"
            cell.dayLabel.text = dateFormater.string(from: date).uppercased()
        }
        
        return cell
    }
}

extension WeatherForecastViewController: UICollectionViewDelegate {

}
