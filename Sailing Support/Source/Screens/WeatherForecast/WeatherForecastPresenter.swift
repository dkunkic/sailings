//
//  WeatherForecastPresenter.swift
//  Sailing Support
//
//  Created by Danijel on 11/02/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation
import MapKit

protocol WeatherForecastProtocol: class {
    func loadWebViews(location: CLLocation)
    func setupCurrentDayItem(weatherItem: WeatherHourItem, locationName: String)
    func setupDays()
}

class WeatherForecastPresenter {

    weak var view: WeatherForecastProtocol?
    var dayItems: [WeatherDayItem]?
    
    func setup() {
        if let location = AppService.sharedService.locationService.location {
            view?.loadWebViews(location: location)
        }
        WeatherService.sharedService.getHourItems().onSuccess(callback: { [weak self] items in
            guard let strongSelf = self else { return }
            if let locationName = WeatherService.sharedService.localizedName {
                strongSelf.view?.setupCurrentDayItem(weatherItem: items[0], locationName: locationName)
            }
            //self.view?.setupWeather(locationName: WeatherService.sharedService.localizedName!)
        }).onFailure { (error) in
            
        }
        if let weatherItems = WeatherService.sharedService.dayItems {
            self.dayItems = weatherItems
            view?.setupDays()
            //view?.setupCurrentDayItem(weatherItem: weatherItems[0], locationName: locationName)
        }
    }
    
}
