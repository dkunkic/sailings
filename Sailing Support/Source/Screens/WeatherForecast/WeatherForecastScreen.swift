//
//  WeatherForecastScreen.swift
//  Sailing Support
//
//  Created by Danijel on 11/02/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation
import UIKit

class WeatherForecastScreen: BaseScreen {
    
    weak var mainDelegate: MainProtocol?
    
    init(mainDelegate: MainProtocol) {
        self.mainDelegate = mainDelegate
    }
    
    required init() {
        fatalError("error init")
    }
    
    override func configureViewController(_ viewController: UIViewController?) {
        if let vc = viewController as? WeatherForecastViewController {
            let presenter = WeatherForecastPresenter()
            vc.presenter = presenter
            vc.mainVCProtocol = self.mainDelegate
            presenter.view = vc
        }
    }
    
    override var storyboardName: String {
        return "App"
    }
    
    override var viewControllerStoryboardId: String {
        return "WeatherForecastViewController"
    }
    
}

