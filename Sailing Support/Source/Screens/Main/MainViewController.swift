//
//  ViewController.swift
//  Sailing Support
//
//  Created by Danijel on 09/11/2017.
//  Copyright © 2017 NELI_IT. All rights reserved.
//

import UIKit
import Alamofire

protocol MainProtocol: class {
    func openScreen(viewController: UIViewController)
    func screenDidLoad(view: UIView)
    func openCloseMenu()
}

class MainViewController: UIViewController {
    
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var menuContainerView: UIView!
    var isMenuOpened: Bool = false
    var animationInProgress: Bool = false
    var menuButton: UIButton!
    var backButton: UIButton!
    
    var presenter: MainPresenter!
    var presentMenuAnimator: PresentMenuAnimator?
    
    var aboutView: UIView?
    
    var adView: AdView!
    
    // MArk: - Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(aboutViewClose(_:)))
//        tap.cancelsTouchesInView = false
//        view.addGestureRecognizer(tap)
        
        self.presenter = MainPresenter(view: self)
        setupNavigation()

        adView = AdView(frame: CGRect(x: 0, y: -200, width: view.frame.width, height: 200))
        view.addSubview(adView)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showAds), name: Notification.Name("showAd"), object: nil)
        
        let edgeGestureRecognizer = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(userSwipedFromEdge(_:)))
        edgeGestureRecognizer.edges = UIRectEdge.left
        edgeGestureRecognizer.delegate = self
        self.view.addGestureRecognizer(edgeGestureRecognizer)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    @objc func showAds() {
        view.bringSubview(toFront: adView)
        
        guard let adId = UserRoute.sharedService.adId else { return }
        
        Alamofire.request(Constants.BASE_URL  + "/MobileMenuCategories/getAd", method: .get, parameters: ["adId": String(adId)]).responseObject { [weak self] (response: DataResponse<AdModel>) in
            guard let strongSelf = self else { return }
            let adModel = response.result.value
            
            strongSelf.adView.images = adModel?.images
            strongSelf.adView.title = adModel?.title
            strongSelf.adView.desc = adModel?.message
            strongSelf.adView.didTapOnMore = {
                let mc = MenuCategory(categoryName: "", categoryId: 1, icon: "")
                mc.locationId = Int(adModel?.locationId ?? "0")
                guard let vc = LocationScreen(mainDelegate: strongSelf, menuItem: mc).instantiateViewController() else { return }
                strongSelf.addChildViewController(vc)
                strongSelf.viewContainer.addSubview(vc.view)
                vc.didMove(toParentViewController: self)
                UserRoute.sharedService.clear()
            }
            
            let slideDown = CGAffineTransform(translationX: 0, y: 264)
            UIView.animate(withDuration: 0.5) {
                strongSelf.adView.transform = slideDown
            }
        }
    }

    // MARK: - Navigation setup
    func setupNavigation() {
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.barTintColor = UIColor.init(red: 19/255, green: 26/255, blue: 34/255, alpha: 1)
        setupNavigationLeftItems()
    }
    
    @objc func aboutViewClose(_ sender: UITapGestureRecognizer) {
        guard let av = aboutView else { return }
        let touchPoint = sender.location(in: self.view)
        if av.frame.contains(touchPoint) {
            return
        }
        av.removeFromSuperview()
        aboutView = nil
    }
    
    @objc func openAbout(_ sender: UIButton) {
        aboutView = UIView(frame: CGRect(x: view.bounds.width - 205, y: 65, width: 200, height: 40))
        aboutView?.backgroundColor =  UIColor.init(red: 60/255, green: 68/255, blue: 78/255, alpha: 1)
        
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 200, height: 40))
        button.titleLabel?.font = UIFont.systemFont(ofSize: 20)
        button.titleLabel?.textAlignment = .left
        button.setTitle("About Us", for: .normal)
        button.titleLabel?.textColor = .white
        button.addTarget(self, action: #selector(showAboutUs(_:)), for: .touchUpInside)
        aboutView?.addSubview(button)
        
        view.addSubview(aboutView!)
    }
    
    @objc func showAboutUs(_ sender: UIButton) {
        guard let av = aboutView else { return }
        av.removeFromSuperview()
        aboutView = nil
        let alert = UIAlertController(title: "About", message: "Sailing Support d.o.o. \n 6974496deed04344", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func setupNavigationLeftItems() {
        
        let titleLabel = UILabel()
        titleLabel.textColor = .white
        titleLabel.textAlignment = .left
        titleLabel.font = UIFont.systemFont(ofSize: 26)
        titleLabel.text = "Sailing Support"
        
        //let labelView = UIBarButtonItem(customView: titleLabel)
        
        menuButton = UIButton(type: .custom)
        menuButton.frame = CGRect(x: 0, y: 0, width: 20, height: 40)
        menuButton.setImage(UIImage(named: "menu"), for: .normal)
        menuButton.imageView?.contentMode = .scaleAspectFit
        menuButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        menuButton.addTarget(self, action: #selector(openMenu(sender:)), for: .touchUpInside)
        menuButton.isUserInteractionEnabled = false
        
        backButton = UIButton(type: .custom)
        backButton.frame = CGRect(x: 0, y: 0, width: 30, height: 40)
        backButton.setImage(UIImage(named: "back")?.withRenderingMode(.alwaysTemplate), for: .normal)
        backButton.imageView?.tintColor = UIColor.white
        backButton.imageView?.contentMode = .scaleAspectFit
        backButton.clipsToBounds = true
        backButton.imageEdgeInsets = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
        backButton.addTarget(self, action: #selector(didTapOnBackButton), for: .touchUpInside)
        backButton.isUserInteractionEnabled = false
        
        let leftItem = UIBarButtonItem(customView: menuButton)
        let backItem = UIBarButtonItem(customView: backButton)
        
        self.navigationItem.leftBarButtonItems = [backItem]
        self.navigationItem.titleView = titleLabel
        self.navigationItem.rightBarButtonItems = [leftItem]
    }
    
    func setupSideMenu() {
        self.menuContainerView = UIView(frame: CGRect(x: self.view.frame.width, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        menuContainerView.backgroundColor = UIColor.clear
        
        let sb = UIStoryboard(name: "Main", bundle: Bundle.main)
        if let vc = sb.instantiateViewController(withIdentifier: "MenuViewController") as? MenuViewController {
            vc.mainProtocol = self
            addChildViewController(vc)
            self.menuContainerView.addSubview(vc.view)
            vc.didMove(toParentViewController: self)
            self.view.addSubview(menuContainerView)
        }
    }
    
    @objc func openMenu(sender: AnyObject) {
        
        openCloseMenu()
    }
    
    @objc func didTapOnBackButton() {
        for childController in childViewControllers {
            if let _ = childController as? MenuViewController {
            } else {
                childController.view.removeFromSuperview()
                childController.removeFromParentViewController()
            }
        }
        
        guard let vc = HomeScreen(mainDelegate: self).instantiateViewController() else { return }
        addChildViewController(vc)
        viewContainer.addSubview(vc.view)
        vc.didMove(toParentViewController: self)
    }
    
    func openCloseMenu() {
        if animationInProgress { return }
        animationInProgress = true
        UIView.animate(withDuration: 0.5, animations: {
            self.menuContainerView.frame = CGRect(x: (self.isMenuOpened ? self.view.frame.width : 0), y: 0, width: self.menuContainerView.frame.width, height: self.menuContainerView.frame.height)
        }, completion: { finished in
            if finished { self.animationInProgress = false }
        })
        self.isMenuOpened = !self.isMenuOpened
    }
    
    // MARK: - Side Menu setup
    let interactor = Interactor()
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? MenuViewController {
            destinationViewController.transitioningDelegate = self
            destinationViewController.interactor = interactor
            destinationViewController.mainProtocol = self
        }
    }
    
    @objc func userSwipedFromEdge(_ sender: UIScreenEdgePanGestureRecognizer) {
        switch sender.state {
        case .began:
            openCloseMenu()
        default:
            break
        }
    }
    
}

// MARK: - MainPresenterProtocol
extension MainViewController: MainPresenterProtocol {
    
    func downloadComplete() {
        self.activityIndicator.isHidden = true
        if let locationId = UserRoute.sharedService.locationId {
            let mc = MenuCategory(categoryName: "", categoryId: 1, icon: "")
            mc.locationId = Int(locationId)
            guard let vc = LocationScreen(mainDelegate: self, menuItem: mc).instantiateViewController() else { return }
            addChildViewController(vc)
            viewContainer.addSubview(vc.view)
            vc.didMove(toParentViewController: self)
            UserRoute.sharedService.clear()
        } else {
            guard let vc = HomeScreen(mainDelegate: self).instantiateViewController() else { return }
            addChildViewController(vc)
            viewContainer.addSubview(vc.view)
            vc.didMove(toParentViewController: self)
        }
    }
    
    func menuDownloadComplete() {
        menuButton.isUserInteractionEnabled = true
        backButton.isUserInteractionEnabled = true
        self.setupSideMenu()
    }
    
}

// MARK: - MainProtocol
extension MainViewController: MainProtocol {

    func openScreen(viewController: UIViewController) {
        for childController in childViewControllers {
            if let _ = childController as? MenuViewController {
            } else {
                childController.view.removeFromSuperview()
                childController.removeFromParentViewController()
            }
        }
        addChildViewController(viewController)
        viewContainer.addSubview(viewController.view)
        viewController.didMove(toParentViewController: self)
    }
    
    func screenDidLoad(view: UIView) {
        if let presentMA = self.presentMenuAnimator {
            presentMA.changeSnapshot(view: view)
        }
    }
    
}

protocol PresentMenuProtocol {
    func updateSnapshotContainer()
}

// MARK: - UIViewControllerTransitioningDelegate
extension MainViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.presentMenuAnimator = PresentMenuAnimator()
        return self.presentMenuAnimator
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DismissMenuAnimator()
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactor.hasStarted ? interactor : nil
    }
}

extension UIImage {
    
    func imageResize (sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        self.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    
}
