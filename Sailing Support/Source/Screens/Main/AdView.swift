//
//  AdView.swift
//  Sailing Support
//
//  Created by Danijel Kunkic on 24/03/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import UIKit
import SDWebImage

class AdView: UIView {
    
    var imageView: UIImageView!
    var titleLabel: UILabel!
    var descLabel: UILabel!
    var xButton: UIButton!
    var moreButton: UIButton!
    var timer: Timer?
    var imageIndex: Int = 0
    
    var images: [AdImages]? {
        didSet {
            guard let _ = images else {
                isImageVisible = false
                return
            }
            isImageVisible = true
            timer?.invalidate()
            timer = Timer.scheduledTimer(withTimeInterval: 2, repeats: true, block: timerChange)
        }
    }
    
    var didTapOnMore: (() -> Void)?
    
    var title: String? {
        didSet {
            titleLabel.text = title
        }
    }
    
    var desc: String? {
        didSet {
            descLabel.text = desc
        }
    }
    
    var isImageVisible: Bool = false {
        didSet {
            let tempFrame = CGRect(x: 10, y: 10, width: isImageVisible ? frame.width / 2.3 : 0, height: frame.height - 50)
            imageView.isHidden = !isImageVisible
            imageView.frame = tempFrame
            
            let tempFrameTitle = CGRect(x: isImageVisible ? (frame.width / 2.3) + 20 : 10, y: 10, width: isImageVisible ? (frame.width - 55) - (frame.width / 2.3) + 20 : frame.width - 60, height: 25)
            titleLabel.frame = tempFrameTitle
            
            let tempFrameDesc = CGRect(x: isImageVisible ? (frame.width / 2.3) + 20 : 10, y: 40, width: isImageVisible ? (frame.width - 55) - (frame.width / 2.3) + 20  : frame.width - 60, height: frame.height - 35 - 40)
            descLabel.frame = tempFrameDesc
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        
        backgroundColor = UIColor(red: 35/255, green: 44/255, blue: 56/255, alpha: 1)
        
        xButton = UIButton(frame: CGRect(x: self.frame.width-50, y: 5, width: 50, height: 50))
        xButton.addTarget(self, action: #selector(closeSelf), for: .touchUpInside)
        xButton.setImage(UIImage(named: "close_icon")?.withRenderingMode(.alwaysTemplate), for: .normal)
        xButton.imageView?.tintColor = UIColor.white
        addSubview(xButton)
        
        imageView = UIImageView(frame: CGRect(x: 10, y: 10, width: frame.width / 2.3, height: frame.height - 50))
        imageView.contentMode = .scaleAspectFill
        addSubview(imageView)
        
        titleLabel = UILabel(frame: CGRect(x: (frame.width / 2.4) + 20, y: 10, width: (frame.width / 2.3) + 20 - frame.width - 55, height: 25))
        titleLabel.font = UIFont.systemFont(ofSize: 22)
        titleLabel.numberOfLines = 0
        titleLabel.textColor = UIColor.white
        addSubview(titleLabel)
        
        descLabel = UILabel(frame: CGRect(x: (frame.width / 2.4) + 20, y: 40, width: (frame.width / 2.3) + 20 - frame.width - 55, height: frame.height - 35 - 40))
        descLabel.font = UIFont.systemFont(ofSize: 18)
        descLabel.textColor = UIColor.white
        descLabel.numberOfLines = 0
        addSubview(descLabel)
        
        moreButton = UIButton(frame: CGRect(x: self.frame.width - 90, y: frame.height - 40, width: 80, height: 30))
        moreButton.addTarget(self, action: #selector(goToLocation), for: .touchUpInside)
        moreButton.backgroundColor = UIColor(red: 53 / 255, green: 61 / 255, blue: 72 / 255, alpha: 1)
        moreButton.setTitle("More  >", for: .normal)
        moreButton.setTitleColor(UIColor(red: 80/255, green: 182/255, blue: 255/255, alpha: 1), for: .normal)
        addSubview(moreButton)
    }
    
    @objc func closeSelf() {
        timer?.invalidate()
        let slideUp = CGAffineTransform(translationX: 0, y: -200)
        UIView.animate(withDuration: 0.5) {
            self.transform = slideUp
        }
    }
    
    @objc func goToLocation() {
        didTapOnMore?()
        closeSelf()
    }
    
    func timerChange(timer: Timer) {
        if imageIndex == images?.count {
            imageIndex = 0
        }
        guard let imageUrl = images?[imageIndex].imgSrc else { return }
        imageView.sd_setImage(with: URL(string: String(format: "http://sailing.coreaplikacije.hr/%@", imageUrl.replacingOccurrences(of: "_90.", with: "_300."))), completed: nil)
        imageIndex += 1
    }
    
}
