//
//  MainPresenter.swift
//  Sailing Support
//
//  Created by Danijel on 03/12/2017.
//  Copyright © 2017 NELI_IT. All rights reserved.
//

import Foundation

protocol MainPresenterProtocol: class {
    func downloadComplete()
    func menuDownloadComplete()
}

class MainPresenter: LocationServiceProtocol {
    
    weak var view: MainPresenterProtocol?
    
    var isDownloadStarted = false
    
    init(view: MainPresenterProtocol) {
        self.view = view
        
        AppService.sharedService.locationService.startUpdatingLocation(locProtocol: self)
        
    }
    
    func locationFetched() {
        if isDownloadStarted {
            return
        }
        
//        guard let status = Network.reachability?.status else { return }
//        switch status {
//        case .unreachable:
//            view?.menuDownloadComplete()
//            view?.downloadComplete()
//            return
//        case .wifi:
//            break
//        case .wwan:
//            break
//        default:
//            break
//        }
        
        isDownloadStarted = true
        AppService.sharedService.getLastVersion()
            .onSuccess(callback: { json in
                guard let newVersionString = json["num"].rawString() else { return }
                guard let newVersion = Int(newVersionString) else { return }
                guard let oldVersion = AppService.sharedService.app.lastVersion else {
                    self.appNeedUpdate(newVersion: newVersion)
                    return
                }
                if oldVersion < newVersion {
                    self.appNeedUpdate(newVersion: newVersion)
                } else {
                    self.menuItemsLoaded()
                }
            })
            .onFailure(callback: { error in
            }
        )
    }
    
    func appNeedUpdate(newVersion: Int) {
        AppService.sharedService.app.lastVersion = newVersion
        AppService.sharedService.saveApp()
        
        self.downloadMenuItems()
    }
    
    func downloadMenuItems() {
        MenuItemsService.sharedService.downloadMenuItems()
            .onSuccess(callback: { menuItems in
                AppService.sharedService.app.menuItems = menuItems
                AppService.sharedService.saveApp()
                self.menuItemsLoaded()
            })
            .onFailure(callback: { error in })
    }
    
    func menuItemsLoaded() {
        WeatherService.sharedService.downloadWeatherData().onSuccess { isDownloadCompleted in
            LocationsService.sharedService.downloadLocationsData().onSuccess(callback: { completed in
                self.view?.downloadComplete()
                self.view?.menuDownloadComplete()
            })
            }.onFailure(callback: { error in
                self.view?.menuDownloadComplete()
            })
    }
    
}
