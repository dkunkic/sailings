//
//  NavigationSailingViewController.swift
//  Sailing Support
//
//  Created by Danijel on 17/02/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import MapKit
import GoogleMaps

class NavigationSailingViewController: BaseViewController {
    
    @IBOutlet weak var compassImageView: UIImageView!
    
    @IBOutlet weak var degreesLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var latLabel: UILabel!
    @IBOutlet weak var lonLabel: UILabel!
    
    @IBOutlet weak var currentSpeedLabel: UILabel!
    @IBOutlet weak var currentSpeedMilesLabel: UILabel!
    
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var mapHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var pickerView: UIPickerView!
    var presenter: NavigationSailingPresenter!
    var locationManager: CLLocationManager!
    
    var coordinatesRoute = [CLLocationCoordinate2D]()
    
    var sisMapExtendet: Bool = false
    
    let mapType = ["Hybrid", "Normal", "Satellite", "Terrain"]
    var distance: Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingHeading()
        locationManager.startUpdatingLocation()
        
        setupPickerView()
        mapView.delegate = self
    }
    
    @IBAction func didTapOnZoomButton(_ sender: Any) {
        mapHeightConstraint.constant = sisMapExtendet ? 300 : UIScreen.main.bounds.height
        scrollView.setContentOffset(CGPoint(x: 0, y: sisMapExtendet ? 300 : 750), animated: true)
        sisMapExtendet = !sisMapExtendet
    }
    
    @IBAction func didTapOnMapBoxButton(_ sender: Any) {
        pickerView.isHidden = false
    }
    
    @IBAction func didTapOnDeleteButton(_ sender: Any) {
        mapView.clear()
        coordinatesRoute.removeAll()
        distance = 0.0
    }
    
    @IBAction func didTapOnMyLocationButton(_ sender: Any) {
        if let location = AppService.sharedService.locationService.location {
            let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 14.0)
            
            self.mapView?.animate(to: camera)
            
            //Finally stop updating location otherwise it will come again and again in this delegate
            AppService.sharedService.locationService.stopUpdatingLocation()
        }
    }
    
    func setupPickerView() {
        pickerView = UIPickerView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height - 300, width: self.view.bounds.width, height: 300))
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.backgroundColor = .white
        pickerView.isHidden = true
        view.addSubview(pickerView)
    }
}

extension NavigationSailingViewController: NavigationSailingProtocol {
    
}

extension NavigationSailingViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        if newHeading.headingAccuracy < 0 { return }
        
        let heading = newHeading.trueHeading

        let rotation = CGFloat(heading/180 * Double.pi)

        UIView.animate(withDuration: 0.5) {
            self.compassImageView.transform = CGAffineTransform(rotationAngle: -rotation)
        }
        
        degreesLabel.text = String(format: "%.1f°", rotation)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let coordinates = manager.location?.coordinate {
            latLabel.text = coordinates.dms.latitude
            lonLabel.text = coordinates.dms.longitude
            //coordinatesRoute.append(coordinates)
        }
        if let location = manager.location {
            let geoCoder = CLGeocoder()
            geoCoder.reverseGeocodeLocation(location) { (placemarks, _) in
                guard let addressDict = placemarks?[0].addressDictionary else { return }
                guard let formatAddress = addressDict["FormattedAddressLines"] as? [String] else { return }
                self.locationLabel.text = formatAddress.joined(separator: ", ")
            }
            let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 10.0)
            
            self.mapView?.animate(to: camera)
            mapView.mapType = .satellite
            mapView.isMyLocationEnabled = true
        }
        
        if let speed = locationManager.location?.speed {
            currentSpeedLabel.text = String(format: "%.2f km/h", (speed == -1) ? 0.00 : speed * 0.277778)
            currentSpeedMilesLabel.text = String(format: "%.2f knots", (speed == -1) ? 0.00 : speed * 0.514444)
        }
    }
    
}

extension NavigationSailingViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        mapView.clear()
        distance = 0.0
        if coordinatesRoute.count == 0 {
            if let location = AppService.sharedService.locationService.location {
                print("user coordinatesRoute")
                coordinatesRoute.append(location.coordinate)
            }
        }
        coordinatesRoute.append(coordinate)
        let path = GMSMutablePath()
        print("coordinatesRoute count ", coordinatesRoute.count)
        for (index, c) in coordinatesRoute.enumerated() {
            path.add(c)
            
            let locationOne = CLLocation(latitude: c.latitude, longitude: c.longitude)
            if coordinatesRoute.count != index + 1 {
                let locationTwo = CLLocation(latitude: coordinatesRoute[index + 1].latitude, longitude: coordinatesRoute[index + 1].longitude)
                distance += locationOne.distance(from: locationTwo)
            }
        }
        let rec = GMSPolyline(path: path)
        rec.strokeColor = .red
        rec.map = mapView
        
        let marker = GMSMarker(position: coordinate)
        marker.opacity = 0.0
        marker.map = mapView
        mapView.selectedMarker = marker
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        let infoView = UIView(frame: CGRect(x: 0, y: 0, width: 140, height: 25))
        infoView.backgroundColor = .white
        let label = UILabel(frame: infoView.frame)
        label.font = UIFont.systemFont(ofSize: 12)
        infoView.addSubview(label)
        var rideTime = ""
        if let location = AppService.sharedService.locationService.location {
            var speed = (location.speed * 3.6 * 100) / 100
            if speed < 1 {
                speed = 1
            }
            let totalTime = (distance/1000)/speed*60
            let hours = totalTime / 60
            let minutes = totalTime.truncatingRemainder(dividingBy: 60)
            rideTime = String(format: "%.0f:%.0f", hours, minutes)
        }
        
        label.text = String(format: "  %.1f km  %.1f NM %@", (distance / 1000), (distance / 1000) * (540/1000), rideTime)
        return infoView
    }
}

extension NavigationSailingViewController: UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return mapType.count
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let type = mapType[row]
        switch type {
        case "Hybrid":
            mapView.mapType = .hybrid
        case "Normal":
            mapView.mapType = .normal
        case "Satellite":
            mapView.mapType = .satellite
        case "Terrain":
            mapView.mapType = .terrain
        default:
            mapView.mapType = .satellite
        }
        pickerView.isHidden = true
    }
}

extension NavigationSailingViewController: UIPickerViewDataSource {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return mapType[row]
    }
}

extension CGFloat {
    var toRadians: CGFloat { return self * .pi / 180 }
    var toDegrees: CGFloat { return self * 180 / .pi }
}

extension CLLocationCoordinate2D {
    var latitudeMinutes: Double {
        return (latitude * 3600).truncatingRemainder(dividingBy: 3600) / 60
    }
    var latitudeSeconds: Double {
        return (latitude * 3600)
            .truncatingRemainder(dividingBy: 3600)
            .truncatingRemainder(dividingBy: 60)
    }
    
    var longitudeMinutes: Double {
        return (longitude * 3600).truncatingRemainder(dividingBy: 3600) / 60
    }
    var longitudeSeconds: Double {
        return (longitude * 3600)
            .truncatingRemainder(dividingBy: 3600)
            .truncatingRemainder(dividingBy: 60)
    }
    
    var dms:(latitude: String, longitude: String) {
        return (String(format: "%d° %d' %.4f\" %@",
                       Int(abs(latitude)),
                       Int(abs(latitudeMinutes)),
                       abs(latitudeSeconds),
                       latitude >= 0 ? "N" : "S"),
                String(format: "%d° %d' %.4f\" %@",
                       Int(abs(longitude)),
                       Int(abs(longitudeMinutes)),
                       abs(longitudeSeconds),
                       longitude >= 0 ? "E" : "W"))
    }
}
