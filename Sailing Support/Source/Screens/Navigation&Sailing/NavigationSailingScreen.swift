//
//  NavigationSailingScreen.swift
//  Sailing Support
//
//  Created by Danijel on 17/02/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation
import UIKit

class NavigationSailingScreen: BaseScreen {
    
    override func configureViewController(_ viewController: UIViewController?) {
        if let vc = viewController as? NavigationSailingViewController {
            let presenter = NavigationSailingPresenter()
            vc.presenter = presenter
            presenter.view = vc
        }
    }
    
    override var storyboardName: String {
        return "App"
    }
    
    override var viewControllerStoryboardId: String {
        return "NavigationSailingViewController"
    }
    
}
