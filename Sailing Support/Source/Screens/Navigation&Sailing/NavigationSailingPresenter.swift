//
//  NavigationSailingPresenter.swift
//  Sailing Support
//
//  Created by Danijel on 17/02/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation

protocol NavigationSailingProtocol: class {
    
}

class NavigationSailingPresenter {
    
    weak var view: NavigationSailingProtocol?
    
}
