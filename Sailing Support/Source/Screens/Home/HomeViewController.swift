//
//  TestViewController.swift
//  Sailing Support
//
//  Created by Danijel on 01/12/2017.
//  Copyright © 2017 NELI_IT. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import SDWebImage

class WeatherItemCell: UICollectionViewCell {
    
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var hoursLabel: UILabel!
    
}

class LocationModelCell: UICollectionViewCell {
    var locationModel: LocationModel?
    @IBOutlet weak var imageVIew: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextView!
    @IBOutlet weak var readMoreButton: UIButton!
    @IBOutlet weak var containerView: UIView!
}

class HomeViewController: BaseViewController, HomeProtocol {
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var locationTitle: UILabel!
    @IBOutlet weak var weatherTitle: UILabel!
    @IBOutlet weak var weatherCollectionView: UICollectionView!
    
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var temperatureSmallLabel: UILabel!
    @IBOutlet weak var temperatureFeelsLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    
    @IBOutlet weak var googleMapView: GMSMapView!
    
    @IBOutlet weak var locationsCollectionView: UICollectionView!
    @IBOutlet weak var myLocationButton: UIButton!
    @IBOutlet weak var googleContainerView: UIView!
    
    var pickerView: UIPickerView!
    var timer: Timer?
    
    let mapType = ["Hybrid", "Normal", "Satellite", "Terrain"]
    
    var presenter: HomePresenter?
    var didScroll: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter?.setup()
        setupGoogleMapView()
        setupPickerView()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer?.invalidate()
    }
    
    func setupWeather(locationName: String) {
        locationTitle.text = locationName
    }
    
    func setupWeatherHours(items: [WeatherHourItem]) {
        let firstItem = items[0]
        weatherTitle.text = firstItem.iconPhrase
        if let icon = firstItem.weatherIcon {
            weatherImageView.image = UIImage(named: String(format: "w_%d", icon))
        }
        if let tempValue = firstItem.temperature?.value {
            temperatureLabel.text = String(format: "%.1f ℃", tempValue)
            temperatureSmallLabel.text = String(format: "%.1f ℃", tempValue)
        }
        if let feelsTempValue = firstItem.realFeelTemperature?.value {
            temperatureFeelsLabel.text = String(format: "Feels like %.1f ℃", feelsTempValue)
        }
        if let windSpeed = firstItem.wind?.speed?.value {
            if let windDirectionAngle = firstItem.wind?.direction?.degrees {
                if let windDirection = firstItem.wind?.direction?.localized {
                    windLabel.text = String(format: "%.1f km/h %.1f ° %@", windSpeed, windDirectionAngle, windDirection)
                }
            }
        }
        if let humidity = firstItem.relativeHumidity {
            humidityLabel.text = String(format: "Humidity: %d %%", humidity)
        }
        
        setupWeatherCollectionView()
    }
    
    func setupWeatherCollectionView() {
        weatherCollectionView.tag = 0
        weatherCollectionView.dataSource = self
        weatherCollectionView.delegate = self
    }
    
    func setupLocationModels() {
        locationsCollectionView.tag = 1
        locationsCollectionView.dataSource = self
        locationsCollectionView.delegate = self
        timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
    }
    
    var w: CGFloat = 0.0
    @objc func scrollAutomatically(_ timer1: Timer) {
        let initailPoint = CGPoint(x: w, y: 0)

        if __CGPointEqualToPoint(initailPoint, locationsCollectionView.contentOffset) {
            if w < locationsCollectionView.contentSize.width {
                w += 0.5
            } else {
                w = -self.view.frame.size.width
            }

            let offsetPoint = CGPoint(x: w, y: 0)

            locationsCollectionView.contentOffset = offsetPoint

        } else {
            w = locationsCollectionView.contentOffset.x
        }
        
    }
    
    func setupPickerView() {
        pickerView = UIPickerView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height - 300, width: self.view.bounds.width, height: 300))
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.backgroundColor = .white
        pickerView.isHidden = true
        view.addSubview(pickerView)
    }
    
    @IBOutlet weak var googleContainerHeightConstraint: NSLayoutConstraint!
    func setupGoogleMapView() {
        if let location = AppService.sharedService.locationService.location {
            let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 10.0)
            
            self.googleMapView?.animate(to: camera)
            
            //Finally stop updating location otherwise it will come again and again in this delegate
            AppService.sharedService.locationService.stopUpdatingLocation()
        }
        
        googleMapView.isUserInteractionEnabled = false
        googleMapView.mapType = .satellite
        googleMapView.isMyLocationEnabled = true
        googleMapView.delegate = self
        googleContainerHeightConstraint.constant = UIScreen.main.bounds.height - 65
        
        myLocationButton.imageView?.contentMode = .scaleAspectFit
        refreshMarkers()
    }
    
    func refreshMarkers() {
        googleMapView.clear()
        let region = googleMapView.projection.visibleRegion()
        let bounds = GMSCoordinateBounds(region: region)
        
        let markersInside = AppService.sharedService.app.locationModels?.filter({ (location) -> Bool in
            guard let lat = location.lat,
                let long = location.lon else {
                    return false
            }
            
            let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
            if bounds.contains(coordinate) {
                return true
            }
            return false
        })
        
        guard let strongMarkers = markersInside else { return }
        let randomMarkers = strongMarkers[randomPick: strongMarkers.count < 200 ? strongMarkers.count : 200]
        
        OperationQueue.main.addOperation {
            var markers = [String: UIImage]()
            
            for locationModel in randomMarkers {
                if let lat = locationModel.lat, let lon = locationModel.lon {
                    if let icon = locationModel.icon {
                        let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(lon)))
                        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                        if markers.contains(where: { (key, value) -> Bool in
                            return key == String(format: "ic_%@_white", icon)
                        }) {
                            imageView.image = markers[String(format: "ic_%@_white", icon)]
                        } else {
                            let image = UIImage(named: String(format: "ic_%@_white", icon))
                            imageView.image = image
                            markers[String(format: "ic_%@_white", icon)] = image
                        }
                        
                        imageView.contentMode = .scaleAspectFit
                        
                        marker.iconView = imageView
                        
                        marker.tracksViewChanges = false
                        marker.infoWindowAnchor = CGPoint(x: 0.5, y: 0.2)
                        marker.accessibilityLabel = locationModel.locationId
                        marker.map = self.googleMapView
                        
                    }
                }
            }
        }
    }
    
    @IBAction func didTapOnWeatherIcon(_ sender: Any) {
        guard let vc = WeatherForecastScreen(mainDelegate: self.mainVCProtocol!).instantiateViewController() else { return }
        self.mainVCProtocol?.openScreen(viewController: vc)
    }
    
    @IBAction func didTapOnZoomButton(_ sender: Any) {
        scrollView.setContentOffset(CGPoint(x: 0, y: googleMapView.isUserInteractionEnabled ? 300 : 640), animated: true)
        googleMapView.isUserInteractionEnabled = !googleMapView.isUserInteractionEnabled
    }
    
    @IBAction func didTapOnMapBoxButton(_ sender: Any) {
        pickerView.isHidden = false
    }
    
    @IBAction func didTapOnMyLocationButton(_ sender: Any) {
        
        if let location = AppService.sharedService.locationService.location {
            let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 14.0)
            
            self.googleMapView?.animate(to: camera)
            
            //Finally stop updating location otherwise it will come again and again in this delegate
            AppService.sharedService.locationService.stopUpdatingLocation()
        }
        
    }
    
    
}

extension HomeViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView.tag {
        case 0:
            guard let items = presenter?.weatherItems else {
                return 0
            }
            return items.count
        case 1:
            guard let items = presenter?.locationItems else {
                return 0
            }
            return items.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView.tag {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WeatherItemCell", for: indexPath) as! WeatherItemCell // swiftlint:disable:this force_cast
            
            if let weatherItem = presenter?.weatherItems![indexPath.row] {
                if let tempValue = weatherItem.temperature?.value {
                    cell.temperatureLabel.text = String(format: "%.1f ℃", tempValue)
                }
                if let epochDateTime = weatherItem.epochDateTime {
                    let date = Date(timeIntervalSince1970: TimeInterval(epochDateTime))
                    let dateFormater = DateFormatter()
                    dateFormater.dateFormat = "HH:mm"
                    cell.hoursLabel.text = dateFormater.string(from: date)
                }
                if let icon = weatherItem.weatherIcon {
                    cell.weatherIcon.image = UIImage(named: String(format: "w_%d", icon))
                }
            }
            
            return cell
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LocationModelCell", for: indexPath) as! LocationModelCell // swiftlint:disable:this force_cast
            cell.containerView.layer.shadowColor = UIColor.black.cgColor
            cell.containerView.layer.shadowOpacity = 1
            cell.containerView.layer.shadowOffset = CGSize.zero
            cell.containerView.layer.shadowRadius = 5
            if let items = presenter?.locationItems {
                let locationModel = items[indexPath.row]
                if let imageSrcSec = locationModel.imgSrcSec {
                    cell.imageVIew.sd_setImage(with: URL(string: String(format: "http://sailing.coreaplikacije.hr/%@", imageSrcSec.replacingOccurrences(of: "_90.", with: "_160."))), completed: nil)
                }
                if let title = locationModel.title {
                    cell.titleLabel.text = title
                }
                if let intro = locationModel.intro {
                    cell.textField.text = intro
                }
                
            }
            
            return cell
        default:
            return collectionView.dequeueReusableCell(withReuseIdentifier: "WeatherItemCell", for: indexPath) as! WeatherItemCell // swiftlint:disable:this force_cast
        }
        
    }
}

extension HomeViewController: UIPickerViewDataSource {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return mapType[row]
    }
}

extension HomeViewController: UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return mapType.count
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let type = mapType[row]
        switch type {
        case "Hybrid":
            googleMapView.mapType = .hybrid
        case "Normal":
            googleMapView.mapType = .normal
        case "Satellite":
            googleMapView.mapType = .satellite
        case "Terrain":
            googleMapView.mapType = .terrain
        default:
            googleMapView.mapType = .satellite
        }
        pickerView.isHidden = true
    }
}

extension HomeViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 1 {
            if let items = presenter?.locationItems {
                let locationModel = items[indexPath.row]
                if let locationId = locationModel.locationId {
                    let menuCategory = MenuCategory(categoryName: "", categoryId: 0, icon: "")
                    menuCategory.locationId = Int(locationId) ?? 0
                    guard let vc = LocationScreen(mainDelegate: self.mainVCProtocol!, menuItem: menuCategory).instantiateViewController() else { return }
                    self.mainVCProtocol?.openScreen(viewController: vc)
                }
            }
        }
    }
    
}

extension HomeViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
}

extension HomeViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        let id = marker.accessibilityLabel
        if let markerInfoView = MapMarkerInfoView.instanceFromNib() as? MapMarkerInfoView {
            if let i =  AppService.sharedService.app.locationModels?.index(where: { locationModel in
                guard let idSec = locationModel.locationId else { return false }
                return id == idSec
            }) {
                if let locationModel = AppService.sharedService.app.locationModels?[i] {
                    markerInfoView.label.text = locationModel.title
                }
            }
            return markerInfoView
        }
        
        return UIView()
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        let id = marker.accessibilityLabel
        if let i =  AppService.sharedService.app.locationModels?.index(where: { locationModel in
            guard let idSec = locationModel.locationId else { return false }
            return id == idSec
        }) {
            guard let locationModel = AppService.sharedService.app.locationModels?[i] else { return }
            if let categoryId = locationModel.categoryId {
                let menuItem = MenuCategory(categoryName: "", categoryId: Int(categoryId)!, icon: "")
                menuItem.locationId = Int(locationModel.locationId ?? "1")
                guard let vc = LocationScreen(mainDelegate: self.mainVCProtocol!, menuItem: menuItem).instantiateViewController() else { return }
                self.mainVCProtocol?.openScreen(viewController: vc)
            }
        }
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        refreshMarkers()
    }
    
}

extension Array {
    /// Picks `n` random elements (partial Fisher-Yates shuffle approach)
    subscript (randomPick n: Int) -> [Element] {
        var copy = self
        for i in stride(from: count - 1, to: count - n - 1, by: -1) {
            copy.swapAt(i, Int(arc4random_uniform(UInt32(i + 1))))
        }
        return Array(copy.suffix(n))
    }
}

//class CustomMarker: NSObject, GMUClusterItem {
//
//}
