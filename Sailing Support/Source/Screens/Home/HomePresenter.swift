
//
//  TestPresenter.swift
//  Sailing Support
//
//  Created by Danijel on 01/12/2017.
//  Copyright © 2017 NELI_IT. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

protocol HomeProtocol: class {
    func setupWeather(locationName: String)
    func setupWeatherHours(items: [WeatherHourItem])
    func setupLocationModels()
}

class HomePresenter {
    
    weak var view: HomeProtocol?
    var weatherItems: [WeatherHourItem]?
    var locationItems: [LocationModel]?
    
    func setup() {
        setupLocationModels()
        setupWeather()
    }
    
    func setupWeather() {
        WeatherService.sharedService.getHourItems().onSuccess(callback: { [weak self] items in
            guard let strongSelf = self else { return }
            strongSelf.weatherItems = items
            strongSelf.view?.setupWeatherHours(items: items)
            strongSelf.view?.setupWeather(locationName: WeatherService.sharedService.localizedName!)
        }).onFailure { (error) in
            
        }
    }
    
    func setupLocationModels() {
        LocationsService.sharedService.getHomePageLocations().onSuccess {  [weak self] (items) in
            guard let strongSelf = self else { return }
            strongSelf.locationItems = items
            strongSelf.view?.setupLocationModels()
        }
    }
    
}

extension MutableCollection {
    /// Shuffles the contents of this collection.
    mutating func shuffle() {
        let c = count
        guard c > 1 else { return }
        
        for (firstUnshuffled, unshuffledCount) in zip(indices, stride(from: c, to: 1, by: -1)) {
            let d: IndexDistance = numericCast(arc4random_uniform(numericCast(unshuffledCount)))
            let i = index(firstUnshuffled, offsetBy: d)
            swapAt(firstUnshuffled, i)
        }
    }
}

extension Sequence {
    /// Returns an array with the contents of this sequence, shuffled.
    func shuffled() -> [Element] {
        var result = Array(self)
        result.shuffle()
        return result
    }
}

