//
//  App.swift
//  Sailing Support
//
//  Created by Danijel on 03/12/2017.
//  Copyright © 2017 NELI_IT. All rights reserved.
//

import Foundation

class App: NSObject, NSCoding {
    
    var lastVersion: Int?
    var menuItems: [MenuCategory]?
    var locationModels: [LocationModel]?
    
    override init() {
        super.init()
        lastVersion = 0
        menuItems = [MenuCategory]()
        locationModels = [LocationModel]()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init()
        self.lastVersion = (aDecoder.decodeObject(forKey: "lastVersion") as? Int) ?? 0
        self.menuItems = (aDecoder.decodeObject(forKey: "menuCategories") as? [MenuCategory]) ?? [MenuCategory]()
        self.locationModels = (aDecoder.decodeObject(forKey: "locationModels") as? [LocationModel] ?? [LocationModel]())
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.lastVersion, forKey: "lastVersion")
        aCoder.encode(self.menuItems, forKey: "menuCategories")
        aCoder.encode(self.locationModels, forKey: "locationModels")
    }
    
}
