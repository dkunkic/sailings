//
//  AppDelegate.swift
//  Sailing Support
//
//  Created by Danijel on 09/11/2017.
//  Copyright © 2017 NELI_IT. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import GoogleMaps
import Firebase
import UserNotifications
import FirebaseAnalytics
import FirebaseInstanceID
import FirebaseMessaging
import UserNotifications
import Alamofire
import SwiftyJSON

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        GMSServices.provideAPIKey("AIzaSyD-6YE8O0X7pBgbSw4a3-6xeZjRG6uft_4")
        
        do {
            Network.reachability = try Reachability(hostname: "www.google.com")
            do {
                try Network.reachability?.start()
            } catch let error as Network.Error {
                
            } catch {
                
            }
        } catch {
            
        }
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        
        Messaging.messaging().delegate = self
        
        let token = Messaging.messaging().fcmToken
        
        FirebaseApp.configure()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        
        let type = userInfo["type"] as? String ?? ""
        let message = userInfo["message"] as? String ?? ""
        let title = userInfo["title"] as? String ?? ""
        let wikiId = userInfo["wikiId"] as? String ?? ""
        let id = userInfo["id"] as? Int ?? 0
        
        let lat = userInfo["lat"] as? String ?? ""
        let long = userInfo["lon"] as? String ?? ""
        
        let radius = userInfo["radius"] as? String ?? ""
        
        if type == "alert" {
            UserRoute.sharedService.setLocation(locationId: wikiId, message: message)
        } else {
            UserRoute.sharedService.adId = id
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showAd"), object: nil)
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        let type = userInfo["type"] as? String ?? ""
        let message = userInfo["message"] as? String ?? ""
        let title = userInfo["title"] as? String ?? ""
        let wikiId = userInfo["wikiId"] as? String ?? ""
        let id = userInfo["id"] as? Int ?? -1
        
        let lat = userInfo["lat"] as? String ?? ""
        let long = userInfo["lon"] as? String ?? ""
        
        let radius = userInfo["radius"] as? String ?? ""
        
        if type == "alert" {
            UserRoute.sharedService.setLocation(locationId: wikiId, message: message)
        } else {
            if id != -1 {
                UserRoute.sharedService.adId = id
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showAd"), object: nil)
            }
            
        }
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
    }
    
}

extension AppDelegate: MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        let params: Parameters = ["regCode": fcmToken, "androidId": UIDevice.current.identifierForVendor!.uuidString]
        
        Alamofire.request("http://sailing.coreaplikacije.hr/rest/GCMRegistration/registerDevice", method: .get, parameters: params).responseJSON(completionHandler: { response in
            _ = response.map { json in
                let _ = JSON(json)
            }
        })
    }
    
}
