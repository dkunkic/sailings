//
//  RouteModel.swift
//  Sailing Support
//
//  Created by Danijel on 18/02/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation
import ObjectMapper

class RouteModel: NSObject, NSCoding, Mappable {
    
    var routeId: String?
    var name: String?
    var desc: String?

    var points: [RoutePointModel]?
    
    required init?(map: Map) {
        super.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        routeId <- map["routeId"]
        desc <- map["description"]
        name <- map["name"]
        points <- map["points"]
    }
    
    override init() {
        routeId = ""
        name = ""
        desc = ""
        points = [RoutePointModel]()
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.routeId, forKey: "routeId")
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.desc, forKey: "desc")
        aCoder.encode(self.points, forKey: "points")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init()
        
        self.routeId = (aDecoder.decodeObject(forKey: "routeId") as? String) ?? ""
        self.name = (aDecoder.decodeObject(forKey: "name") as? String) ?? ""
        self.desc = (aDecoder.decodeObject(forKey: "desc") as? String) ?? ""
        self.points = (aDecoder.decodeObject(forKey: "points") as? [RoutePointModel]) ?? [RoutePointModel]()
    }
    
    
}
