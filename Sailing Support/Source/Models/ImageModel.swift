//
//  ImageModel.swift
//  Sailing Support
//
//  Created by Danijel Kunkic on 25/02/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation
import ObjectMapper

class ImageModelRoot: Mappable {
    var error: Bool?
    var root: [ImageModel]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        error <- map["error"]
        root <- map["root"]
    }
}

class ImageModel: Mappable {
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        self.imageId <- map["imageId"]
        self.imageUrl <- map["imageUrl"]
        self.imageName <- map["imageName"]
        self.imageDesc <- map["imageDesc"]
    }
    
    
    var imageId: String?
    var imageUrl: String?
    var imageName: String?
    var imageDesc: String?
    
    
    
}
