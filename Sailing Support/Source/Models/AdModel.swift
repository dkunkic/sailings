//
//  AdModel.swift
//  Sailing Support
//
//  Created by Danijel Kunkic on 24/03/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation
import ObjectMapper

class AdModel: Mappable {
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        adId <- map["adId"]
        error <- map["error"]
        locationId <- map["locationId"]
        title <- map["title"]
        message <- map["message"]
        images <- map["images"]
    }
    
    var adId: String?
    var error: Bool?
    var locationId: String?
    var title: String?
    var message: String?
    var images: [AdImages]?
    
}

class AdImages: Mappable {
    
    var imgId: String?
    var imgSrc: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        imgSrc <- map["imgSrc"]
        imgId <- map["imgId"]
    }
}
