//
//  RoutesRootModel.swift
//  Sailing Support
//
//  Created by Danijel on 18/02/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation
import ObjectMapper

class RoutesRootModel: Mappable {
    
    var error: Bool?
    var root: [RouteModel]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        error <- map["error"]
        root <- map["root"]
    }
    
}
