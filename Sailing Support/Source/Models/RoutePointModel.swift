//
//  RoutePointModel.swift
//  Sailing Support
//
//  Created by Danijel on 18/02/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation
import ObjectMapper

class RoutePointModel: NSObject, NSCoding, Mappable {
    
    var id: String?
    var locationId: String?
    var title: String?
    var lat: Double?
    var lon: Double?
    var routeId: String?
    
    override init() {
        id = ""
        locationId = ""
        title = ""
        lat = 0.0
        lon = 0.0
        routeId = ""
    }
    
    required init?(map: Map) {
        super.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        locationId <- map["locationId"]
        title <- map["title"]
        lat <- map["lat"]
        lon <- map["lon"]
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.locationId, forKey: "locationId")
        aCoder.encode(self.title, forKey: "title")
        aCoder.encode(self.lat, forKey: "lat")
        aCoder.encode(self.lon, forKey: "lon")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init()
        self.id = (aDecoder.decodeObject(forKey: "") as? String) ?? ""
        self.locationId = (aDecoder.decodeObject(forKey: "locationId") as? String) ?? ""
        self.title = (aDecoder.decodeObject(forKey: "title") as? String) ?? ""
        self.lat = (aDecoder.decodeObject(forKey: "lat") as? Double) ?? 0.0
        self.lon = (aDecoder.decodeObject(forKey: "lon") as? Double) ?? 0.0
    }
    
}
