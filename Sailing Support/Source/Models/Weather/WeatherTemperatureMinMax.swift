//
//  WeatherTemperatureMinMax.swift
//  Sailing Support
//
//  Created by Danijel on 14/02/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation

class WeatherTemperatureMinMax: NSObject {
    
    var minimum: WeatherTemperature?
    var maximum: WeatherTemperature?
    
}
