//
//  WeatherWindSpeed.swift
//  Sailing Support
//
//  Created by Danijel on 14/01/2018.
//  Copyright © 2018 NELI_IT. All rights reserved.
//

import Foundation

class WeatherWindSpeed: NSObject {
    
    var value: Float?
    var unit: String?
    var unitType: Int?
    
    init(value: Float, unit: String, unitType: Int) {
        self.value = value
        self.unit = unit
        self.unitType = unitType
    }
    
}
